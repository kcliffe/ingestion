using TaskExec.Infrastructure.Execution;

namespace TaskExec.Workers
{
	/*
	 Host interface:
	 */

    // TODO. Look at thread safety requirements
    public class WorkerContext
    {
        public string ExecName { get; set; }
        public Signal Signal { get; set; }
		public string InputQueue { get; set; }
		public IInvokeTasks TaskInvoker { get; set; }
        // consuming queue(s)
        // ...
    }
}
