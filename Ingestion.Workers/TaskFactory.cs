﻿using System;
using Ingestion.Infrastructure;

namespace Ingestion.Workers
{
	/// <summary>
	/// Return an implementation for a given Task
	/// </summary>
	public class TaskFactory : ITaskFactory
	{
		public TaskFactory ()
		{
		}

		public ITask GetTaskImplementation (TaskDefinition message)
		{
			ITask task = null;

			// TODO, type when polymorhpic
			switch (message.Category) {
				case IngestionConsts.Categories.CreateRelationship:
					task = new CreateRelationshipTask ();
					break;
				default:
					throw new Exception ("Unknown task " + message.Id);
			}

			return task;
		}
	}
}

