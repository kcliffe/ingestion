#bin/bash

tab="--tab"
foo=""

for i in 1 2 3; do
      worker_name=$(($1+i))
      #echo "fc_$worker_name"
      cmd="bash -c './bin/Debug/Ingestion.Workers.exe fc_$worker_name';bash"
      foo+=($tab -e "$cmd")         
done

gnome-terminal "${foo[@]}"

exit 0
