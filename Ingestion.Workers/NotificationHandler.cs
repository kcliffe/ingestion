﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Management;
using TaskExec.Infrastructure.Transport;

namespace TaskExec.Workers
{
    /// <summary>
    /// A bit of developer w**k I suppose. Makes it posible to create classes like 
    /// <code>
    /// public class Handler<MessageType> {
    ///   public void Handle(MessageType message) {
    ///     .. impl...
    ///   } 
    /// }
    /// 
    /// instead of giant ugly switch() {} statements on the message type. Currently lacks nce stuff like IOC (would be pretty simple)
    /// </code>
    /// </summary>
    public class NotificationReceiver
    {
        private static Dictionary<Type, Type> _handlers; // message type, handler type
		//private readonly IInvokeTasks _taskInvoker;

        public NotificationReceiver(IInvokeTasks taskInvoker)
        {            
			//_taskInvoker = taskInvoker;

            // locate all message handlers in the solution. Bit of an anti-pattern (do stuff in ctor). But... hey.
            Discover();
        }

		/// <summary>
		/// Executes a handler when a message arrives
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="hasMessage">Message.</param>
        public void ExecuteHandler(object context, IHasMessage hasMessage)
        {
            // we don't cache. so it's instance per invocation
            Type handlerType;
            if (!_handlers.TryGetValue(hasMessage.GetType(), out handlerType)) return; // or throw?

			var instance = Activator.CreateInstance(handlerType);
			handlerType.InvokeMember("Handle", BindingFlags.InvokeMethod, null, instance, new [] { context, hasMessage }); 
        }

        private static void Discover()
        {
            _handlers = new Dictionary<Type, Type>();

            const string qualifiedInterfaceName = "TaskExec.Workers.INotificationHandler`1"; // TODO. Dangerous hardcode!
            var interfaceFilter = new TypeFilter(InterfaceFilter);

            //var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //var di = new DirectoryInfo(path);
            //foreach (var file in di.GetFiles("*.dll"))
            //{
                try
                {
					// var nextAssembly = Assembly.Retype parameter in the definitiflectionOnlyLoadFrom(file.FullName);
		
                    // for now, skip full assembly scan, we'd need AssemblyResolverBlah...
                    var nextAssembly = Assembly.GetExecutingAssembly();
                    var types = nextAssembly.GetTypes();
                    foreach (var type in types)
                    {
                        var handlerTypes = type.FindInterfaces(interfaceFilter, qualifiedInterfaceName);
						if (handlerTypes.Length <= 0) continue;
						
						handlerTypes.ToList().ForEach(x => {
	                        var m = x.GetGenericArguments().First();
	                        _handlers.Add(m, type);
						});
					}
              
                }
                catch (BadImageFormatException)
                {
                    // Not a .net assembly  - ignore
                }
            //}
        }

        public static bool InterfaceFilter(Type typeObj, Object criteriaObj)
        {
			return typeObj.ToString().StartsWith(criteriaObj.ToString());
        }
    }

	/// <summary>
	/// Handlers which want to be invoked when a message is received implement this interface
	/// </summary>
    public interface INotificationHandler<in TMessage> where TMessage : NotificationBase
    {
		// TODO. Currently cheating while we don't have IOC creating the Handler
		// we currently stick stuff like the TaskInvoker in here. 
		// When we have IOC, Handle could take *any* number of types - the IOC would resolve them
        void Handle(object context, TMessage signalNotificationBase);
    }
}
