﻿using System.Collections.Concurrent;
using NLog;
using System;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Transport;

namespace TaskExec.Workers
{	
	/// <summary>
	/// A host for a generic worker.
	/// </summary>
	class MainClass
	{        
		private static int _workerCount;		
        // TODO. This looks dangerous
	    private static NotificationReceiver notificationReceiver;
        private static readonly WorkerContext WorkerContext = new WorkerContext();
		
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public static void Main (string[] args)
		{            
			WorkerContext.ExecName = args.Length == 0 
				? "Worker" + ++_workerCount
				: args [0];

			Logger.Info("\n!! -------------");
            Logger.Info("Worker {0} starting..", WorkerContext.ExecName);
            Logger.Info("\n!! -------------");

			WorkerContext.InputQueue = Consts.QueueNames.Worker;
			if (args.Length == 1) {
				WorkerContext.InputQueue = args [0];
			}

			WorkerContext.Signal = new Signal ();

		    try
		    {
				//RegisterThisWorker();
                CreateSystemNotificationQueue();
                CreateWorkReceiverQueue();
		    }
		    catch (Exception ex)
		    {		        
		        Logger.Fatal(ex, string.Format(@"Oops. Something bad happened and worker {0} cannot start.
Is the transport (RabbitMq) running?{2}The message was -> '{1}'", WorkerContext.ExecName, ex.Message, Environment.NewLine));
		    }            			
		}

		/// <summary>
		/// Creates the work receiver queue.
		/// </summary>
		static void CreateWorkReceiverQueue ()
		{
			var taskInvoker = new DefaultTaskInvoker(WorkerContext.ExecName, WorkerContext.Signal);
			WorkerContext.TaskInvoker = taskInvoker;

			// consumer for work items 
			TransportFactory.GetConsumer<TaskDefinition>(TaskExecConsts.DefaultHost, WorkerContext.InputQueue, taskInvoker.Execute, WorkerContext.Signal).Consume();        
		}

		/// <summary>
		/// Creates the system notification queue. This queue used to get notifications of events
		/// like Pause, Cancel which will generally be broadcast from the Conductor.
		/// </summary>
		static void CreateSystemNotificationQueue()
		{
            // TODO. as commented somewhere else, the Notification reciever *could* be built into the RabbitEventSubscriber / RabbitEventConsumer
            notificationReceiver = new NotificationReceiver(null);

            // TODO. Const.DefaultHost needs parameterising
			var rabbitEventSubscriber = new RabbitEventSubscriber<IHasMessage> (Consts.DefaultHost, Consts.SystemBackChannelQueueName);

            // this could be built into the RabbitEventSubscriber (polymorphic message delegation, just pass this into the ctor?)
			rabbitEventSubscriber.MessageRetrieved += (sender, e) => notificationReceiver.ExecuteHandler(WorkerContext, e.Message);
			rabbitEventSubscriber.Consume ();

			Logger.Info ("Cancellation subscriber started");
		} 
	}
}
