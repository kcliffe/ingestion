﻿using System;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Management;

namespace TaskExec.Workers.Handlers
{
    /// <summary>
    /// Handler invoked whenever there is a system notification received.
    /// e.g Cancel, Pause, Start etc
    /// </summary>
    public class SystemNotificationHandler : HandlerBase, INotificationHandler<CancelSignalNotification>
    {
        // TODO: currently requires empty ctor?
        //public SystemNotificationHandler()
        //{
        //}

        public void Handle(object context, CancelSignalNotification cancelSignal)
        {
            var workerContext = context as WorkerContext;
            ValidateContext(workerContext);

            // TODO. Specific handlers notifications. Get rid of SignalType.
            if (!WorkerIsTargetOfNotification(workerContext, cancelSignal)) return;

            // let the TaskInvoker know that all tasks for the spepcific Job should be discarded
            workerContext.TaskInvoker.AddFilteredJob(cancelSignal.Message);
            workerContext.Signal.SetSignal(Signal.SignalType.Cancel);
        }
    }
}

