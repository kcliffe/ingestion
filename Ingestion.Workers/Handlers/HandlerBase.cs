﻿using System;
using NLog;
using TaskExec.Infrastructure.Management;

namespace TaskExec.Workers.Handlers
{
    public class HandlerBase
    {        
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected void ValidateContext(WorkerContext workerContext)
        {            
            if (workerContext == null)
            {
                throw new Exception("Ooops. worker context expected");
            }
        }

        protected bool WorkerIsTargetOfNotification(WorkerContext workerContext, SignalNotificationBase signalNotificationBase)
        {
            return signalNotificationBase.AllWorkers ||
                string.CompareOrdinal(signalNotificationBase.Message, workerContext.ExecName) == 0;
        }
    }
}
