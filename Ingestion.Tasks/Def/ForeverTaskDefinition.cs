﻿using System;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	public class ForeverTaskDefinition : TaskDefinition
	{
		public string EchoMessage {	get; set; }

		public ForeverTaskDefinition ()
		{
			this.Id = Guid.NewGuid ().ToString();
		}
	}
}

