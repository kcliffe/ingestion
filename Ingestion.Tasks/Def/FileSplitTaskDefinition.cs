﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	public class FileSplitTaskDefinition : TaskDefinition
	{
		public string SourcePath {
			get; set;
		}

		public string TargetFolder {
			get; set;
		}

		public int LinesPerFile {
			get; set;
		}

		public FileSplitTaskDefinition ()
		{
		}
	}
}

