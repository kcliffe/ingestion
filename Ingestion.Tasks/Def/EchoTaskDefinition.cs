﻿using System;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	public class EchoTaskDefinition : TaskDefinition
	{
		public string EchoMessage {	get; set; }

		public EchoTaskDefinition ()
		{
			this.Id = Guid.NewGuid ().ToString();
		}
	}
}

