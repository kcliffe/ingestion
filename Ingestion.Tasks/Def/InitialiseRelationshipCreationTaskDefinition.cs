﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	/// <summary>
	/// All data required to create 1+ relationships
	/// </summary>
	public class InitialiseRelationshipCreationTaskDefinition : TaskDefinition
	{        
		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
		public InitialiseRelationshipCreationTaskDefinition ()
		{		
		}

		/// <summary>
		/// Path to relationship source file
		/// </summary>
		/// <value>The source path.</value>
		public string SourcePath {
			get;
			set;
		}
	}
}

