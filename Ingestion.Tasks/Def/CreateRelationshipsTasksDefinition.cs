﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	/// <summary>
	/// All data required to create a relationship.
	/// </summary>
	public class CreateRelationshipsTaskDefinition : TaskDefinition
	{
		public string SourcePath { get; set;}

        public bool ShouldThrowBecause()
        {
			// return SourceId < 0;
			return false;
        }

		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
		public CreateRelationshipsTaskDefinition ()
		{
		}
    }
}

