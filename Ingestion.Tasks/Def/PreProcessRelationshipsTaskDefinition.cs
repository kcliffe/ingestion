﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	public class PreProcessRelationshipsTaskDefinition : TaskDefinition
	{
		public string SourcePath {
			get; set;
		}

		public string TargetPath {
			get; set;
		}

		public PreProcessRelationshipsTaskDefinition ()
		{
		}
	}
}

