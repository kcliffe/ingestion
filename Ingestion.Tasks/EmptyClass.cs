﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Wynyard.Xerem.Emit.Test
{
	public class TestConnectorSuite : TestBase
	{
		public const String Namespace = "XeremTestConnectorSample_";

		public enum Directionality : uint
		{
			Either,
			None,
			SourceToTarget,
			TargetToSource,
			Both,
		}

		[Test]
		public void TestConnectorAsRelationship()
		{
			this.TestConnector(true);
		}

		[Test]
		public void TestConnectorAsReference()
		{
			this.TestConnector(false);
		}

		public void TestConnector(Boolean modelAsRelationship)
		{
			using (var guard = Runtime.ExclusiveWriteGuard(_domain))
			{
				var builder = Runtime.GetDomainBuilder(_domain);

				var iObjectBeh = builder.Provision(typeof (TestConnector.BindingInterfaces.IObject));
				var iEntityBeh = builder.Provision(typeof (TestConnector.BindingInterfaces.IEntity));
				var iEventBeh = builder.Provision(typeof(TestConnector.BindingInterfaces.IEvent));
				var iRelationshipBeh = builder.Provision(typeof(TestConnector.BindingInterfaces.IRelationship));
				var iRelationshipTypeBeh = builder.Provision(typeof(TestConnector.BindingInterfaces.IRelationshipType));

				var personType = builder.GetTypeBuilder();
				personType.Name = TestConnectorSuite.Namespace + "Person";
				personType.Attach(iObjectBeh);
				personType.Attach(iEntityBeh);
				var fullnameProp = personType.GetPropertyBuilder();
				fullnameProp.Name = "Fullname";
				fullnameProp.PropertyType = typeof (String);

				var carType = builder.GetTypeBuilder();
				carType.Name = TestConnectorSuite.Namespace + "Car";
				carType.Attach(iObjectBeh);
				carType.Attach(iEntityBeh);
				carType.Attach(iEventBeh); // let it be event for testing purposes
				var timeProp = carType.GetPropertyBuilder();
				timeProp.Name = "Time";
				timeProp.PropertyType = typeof (DateTimeOffset);
				var modelProp = carType.GetPropertyBuilder();
				modelProp.Name = "Model";
				modelProp.PropertyType = typeof(String);

				if (modelAsRelationship)
				{
					var ownershipType = builder.GetTypeBuilder();
					ownershipType.Name = TestConnectorSuite.Namespace + "Ownership";
					ownershipType.Attach(iObjectBeh);
					ownershipType.Attach(iRelationshipBeh);
					ownershipType.Attach(iRelationshipTypeBeh);
					var directionProp = ownershipType.GetPropertyBuilder();
					directionProp.Name = "Direction";
					directionProp.PropertyType = typeof(uint);
					directionProp.IsStatic = true;
					var strengthProp = ownershipType.GetPropertyBuilder();
					strengthProp.Name = "Strength";
					strengthProp.PropertyType = typeof(double);

					var sourceProp = ownershipType.GetPropertyBuilder();
					sourceProp.Name = "Source";
					sourceProp.PropertyType = personType;
					sourceProp.InverseName = "CarsOwned";
					var targetProp = ownershipType.GetPropertyBuilder();
					targetProp.Name = "Target";
					targetProp.PropertyType = carType;
					targetProp.InverseName = "Owners";
				}
				else
				{
					var carsOwnedProp = personType.GetPropertyBuilder();
					carsOwnedProp.Name = "CarsOwned";
					carsOwnedProp.PropertyType = typeof (IEnumerable<>).MakeGenericType(carType);
					carsOwnedProp.InverseName = "Owners";
				}

				builder.Save();
				guard.Complete();
			}

			using (var guard = Runtime.SharedWriteGuard(_domain))
			{
				var carType = Runtime.Compile(TestConnectorSuite.Namespace + "Car", _domain);
				var personType = Runtime.Compile(TestConnectorSuite.Namespace + "Person", _domain);

				var car = carType.CreateInstance();
				car.AsDynamic().Model = "Audi";
				var person = personType.CreateInstance();
				person.AsDynamic().Fullname = "John W.";

				if (modelAsRelationship)
				{
					var ownershipType = Runtime.Compile(TestConnectorSuite.Namespace + "Ownership", _domain);
					ownershipType.GetAttribute<TestConnector.BindingInterfaces.IRelationshipType>().Directionality = Directionality.SourceToTarget;

					var ownership = ownershipType.CreateInstance();
					ownership.AsDynamic().Strength = 0.5;
					ownership.AsDynamic().Source = person.AsDynamic();
					ownership.AsDynamic().Target = car.AsDynamic();
				}
				else
				{
					person.AsDynamic().CarsOwned = Instance.Union(car.AsDynamic()); // (KC) union deals with adding to collection
				}

				guard.Complete();
			}

			using (var guard = Runtime.SharedReadGuard(_domain))
			{
				var log = new StringBuilder();

				#region Static discovery

				log.Append("Data sent to UI on start up:\n");
				foreach (var type in typeof (TestConnector.DataAccessInterfaces.INode).GetTypesAssignableFrom(false))
				{
					var nodeType = type.GetAttribute<TestConnector.DataAccessInterfaces.INodeType>();
					log.AppendFormat("1) Node type: {1}[{0}]: {2}\n",
						nodeType.Id, nodeType.Name, nodeType.Description);

					foreach (var prop in nodeType.PrimitiveProperties)
					{
						log.AppendFormat("1)     Property: {1}[{0}] of type {3}: {2}\n",
							prop.Id, prop.Name, prop.Description, prop.PropertyType);
					}

					foreach (var connectorType in nodeType.ForwardExpandableConnectors)
					{
						log.AppendFormat("1)     Forward expandable connector: {1}[{0}]: {2}\n",
							connectorType.Moniker, connectorType.ForwardName, connectorType.ForwardDescription);

						if (connectorType.AsRelationshipType() != null)
						{
							log.AppendFormat("1)         Represents relationship: {1}[{0}]: {2}\n",
								connectorType.AsRelationshipType().Id,
								connectorType.AsRelationshipType().Name,
								connectorType.AsRelationshipType().Description);
						}
					}

					foreach (var connectorType in nodeType.InverseExpandableConnectors)
					{
						log.AppendFormat("1)     Inverse expandable connector: {1}[{0}]: {2}\n",
							connectorType.Moniker, connectorType.InverseName, connectorType.InverseDescription);

						if (connectorType.AsRelationshipType() != null)
						{
							log.AppendFormat("1)         Represents relationship: {1}[{0}]: {2}\n",
								connectorType.AsRelationshipType().Id,
								connectorType.AsRelationshipType().Name,
								connectorType.AsRelationshipType().Description);
						}
					}
				}

				foreach (var type in typeof(TestConnector.DataAccessInterfaces.IRelationship).GetTypesAssignableFrom(false))
				{
					var relType = type.GetAttribute<TestConnector.DataAccessInterfaces.IRelationshipType>();
					log.AppendFormat("1) Connector with properties (i.e. relationship): {1}[{0}]: {2}\n", 
						relType.Id, relType.Name, relType.Description);

					foreach (var prop in relType.PrimitiveProperties)
					{
						log.AppendFormat("1)     Property: {1}[{0}] of type {3}: {2}\n",
							prop.Id, prop.Name, prop.Description, prop.PropertyType);
					}

				}
				log.Append("\n");

				#endregion

				log.Append("Data sent to UI on node click:\n");
				// KC. Needs repo to filter to types user has selected (via request param - currently relationship type id / event role type id)
				// would be ... 
				foreach (var type in typeof(TestConnector.DataAccessInterfaces.INode).GetTypesAssignableFrom(false))
				{
					foreach (var inst in type.GetInstances<TestConnector.DataAccessInterfaces.INode>())
					{
						log.AppendFormat("--  for Node {0}:\n", inst);

						// KC. GetInstanceProperties()
						foreach (var prop in inst.Type.PrimitiveProperties)
						{                            
							log.AppendFormat("1)     Property: {1}[{0}] of type {3}, value: {4}: {2}\n",
								prop.Id, prop.Name, prop.Description, prop.PropertyType, inst.GetPropertyValue(prop));
						}
						foreach (var connectorType in inst.ForwardExpandableConnectors)
						{
							log.AppendFormat("2)     Forward expandable connector: {1}[{0}]\n",
								connectorType.Moniker, connectorType.ForwardName);
						}
						foreach (var connectorType in inst.InverseExpandableConnectors)
						{
							log.AppendFormat("2)     Inverse expandable connector: {1}[{0}]\n",
								connectorType.Moniker, connectorType.InverseName);
						}
					}
				}
				log.Append("\n");

				log.Append("Data sent to UI on node expand:\n");
				foreach (var type in typeof(TestConnector.DataAccessInterfaces.INode).GetTypesAssignableFrom(false))
				{
					foreach (var inst in type.GetInstances<TestConnector.DataAccessInterfaces.INode>())
					{
						log.AppendFormat("--  for Node {0}:\n", inst);

						foreach (var connectorType in inst.ForwardExpandableConnectors)
						{
							log.AppendFormat("--     by Forward expandable connector: {1}[{0}]\n",
								connectorType.Moniker, connectorType.ForwardName);

							foreach (var connector in inst.ForwardExpand(connectorType))
							{
								log.AppendFormat("3)         Connector: {0} ===> {1}\n",
									connector.Source, connector.Target);

								if (connector.AsRelationship() != null)
								{
									log.AppendFormat("3)             with properties from {0}:\n", connector.AsRelationship());
								}
							}
						}
						foreach (var connectorType in inst.InverseExpandableConnectors)
						{
							log.AppendFormat("--     by Inverse expandable connector: {1}[{0}]\n",
								connectorType.Moniker, connectorType.InverseName);

							foreach (var connector in inst.InverseExpand(connectorType))
							{
								log.AppendFormat("3)         Connector: {0} ===> {1}\n",
									connector.Source, connector.Target);

								if (connector.AsRelationship() != null)
								{
									log.AppendFormat("3)             with properties from {0}:\n", connector.AsRelationship());
								}
							}
						}
					}
				}
				log.Append("\n");

				log.Append("Data sent to UI on connector click:\n");
				foreach (var type in typeof(TestConnector.DataAccessInterfaces.IRelationship).GetTypesAssignableFrom(false))
				{
					foreach (var inst in type.GetInstances<TestConnector.DataAccessInterfaces.IRelationship>())
					{
						log.AppendFormat("--  for Connector {0}:\n", inst);

						foreach (var prop in inst.Type.PrimitiveProperties)
						{
							log.AppendFormat("4)                 {0}: {1}\n", prop.Name, inst.GetPropertyValue(prop));
						}
					}
				}
				log.Append("\n");

				Console.Write(log);
				System.Diagnostics.Trace.Write(log);

				guard.Complete();
			}
		}
	}

	// implementation of Binding Interfaces
	namespace TestConnector.BindingInterfaces
	{
		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IObject")]
		public interface IObject : IEmittedInstance, DataAccessInterfaces.IObject
		{
			new ulong Id { get; }

			[ExtensionProperty(typeof(ObjectImpl))]
			new DataAccessInterfaces.IObjectType Type { get; }

			[ExtensionMethod(typeof(ObjectImpl))]
			new Object GetPropertyValue(DataAccessInterfaces.IObjectProperty property);
			[ExtensionMethod(typeof(ObjectImpl))]
			new void SetPropertyValue(DataAccessInterfaces.IObjectProperty property, Object value);
		}

		public static class ObjectImpl
		{
			public static DataAccessInterfaces.IObjectType get_Type(IObject self)
			{
				return self.GetType().GetAttribute<IObjectType>();
			}

			public static Object GetPropertyValue(IObject self, DataAccessInterfaces.IObjectProperty property)
			{
				return ((IEmittedPropertyAttribute) property).Compile().GetValue(self, null);
			}

			public static void SetPropertyValue(IObject self, DataAccessInterfaces.IObjectProperty property, Object value)
			{
				((IEmittedPropertyAttribute)property).Compile().SetValue(self, value, null);
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IObject")]
		public interface IObjectType : IEmittedTypeAttribute, DataAccessInterfaces.IObjectType
		{
			new ulong Id { get; }
			new string Name { get; }
			new string Description { get; }

			[ExtensionProperty(typeof(ObjectTypeImpl))]
			new IEnumerable<DataAccessInterfaces.IObjectProperty> PrimitiveProperties { get; }
			[ExtensionProperty(typeof(ObjectTypeImpl))]
			new IEnumerable<DataAccessInterfaces.IObjectProperty> ReferenceProperties { get; }
		}

		public static class ObjectTypeImpl
		{
			public static IEnumerable<DataAccessInterfaces.IObjectProperty> get_PrimitiveProperties(IObjectType self)
			{
				return self.Compile().GetProperties(true)
					.Select(i => i.GetAttribute<IObjectProperty>())
					.Where(i => i.IsReference() == false && i.IsStatic == false);
			}

			public static IEnumerable<DataAccessInterfaces.IObjectProperty> get_ReferenceProperties(IObjectType self)
			{
				return self.Compile().GetProperties(true)
					.Select(i => i.GetAttribute<IObjectProperty>())
					.Where(i => i.IsReference());
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IObject")]
		public interface IObjectProperty : IEmittedPropertyAttribute, DataAccessInterfaces.IObjectProperty, DataAccessInterfaces.IConnectorType
		{
			new ulong Id { get; }
			new string Name { get; }
			new string Description { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new Type PropertyType { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new DataAccessInterfaces.IObjectType DeclaringType { get; }

			[ExtensionMethod(typeof(ObjectPropertyImpl))]
			new bool IsReference();

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new string Moniker { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new DataAccessInterfaces.INodeType SourceType { get; }
			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new DataAccessInterfaces.INodeType TargetType { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new string ForwardName { get; }
			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new string InverseName { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new string ForwardDescription { get; }
			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new string InverseDescription { get; }

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new TestConnectorSuite.Directionality Directionality { get; }

			[ExtensionMethod(typeof(ObjectPropertyImpl))]
			new DataAccessInterfaces.IRelationshipType AsRelationshipType();

			[ExtensionProperty(typeof(ObjectPropertyImpl))]
			new bool IsSymmetrical { get; }

			[ExtensionMethod(typeof(ObjectPropertyImpl))]
			new bool Equals(DataAccessInterfaces.IConnectorType other);
		}

		public static class ObjectPropertyImpl
		{
			public static Type get_PropertyType(IObjectProperty self)
			{
				return self.Compile().PropertyType;
			}

			public static DataAccessInterfaces.IObjectType get_DeclaringType(IObjectProperty self)
			{
				return self.Compile().DeclaringType.GetAttribute<IObjectType>();
			}

			public static bool IsReference(IObjectProperty self)
			{
				return typeof (IEmittedInstance).IsAssignableFrom(self.Compile().PropertyType)
					|| typeof (IEnumerable<IEmittedInstance>).IsAssignableFrom(self.Compile().PropertyType);
			}

			public static string get_Moniker(IObjectProperty self)
			{
				return String.Format("{0}:{1}", self.DeclaringType.Id, self.Id);
			}

			public static DataAccessInterfaces.INodeType get_SourceType(IObjectProperty self)
			{
				return self.Compile().DeclaringType.GetAttribute<INodeType>();
			}

			public static DataAccessInterfaces.INodeType get_TargetType(IObjectProperty self)
			{
				return self.Compile().PropertyType.GetAttribute<INodeType>();
			}

			public static string get_ForwardName(IObjectProperty self)
			{
				return self.Name;
			}

			public static string get_InverseName(IEmittedPropertyAttribute self)
			{
				return self.InverseName;
			}

			public static string get_ForwardDescription(IObjectProperty self)
			{
				return self.Description;
			}

			public static string get_InverseDescription(IObjectProperty self)
			{
				return String.Format("Inverse of the reference {0} by {1} ({2})",
					self.ForwardName, self.DeclaringType.Name, self.Description);
			}

			public static TestConnectorSuite.Directionality get_Directionality(IObjectProperty self)
			{
				// only a relationship has got persisted Direction property
				// properties do not have persisted properties
				return TestConnectorSuite.Directionality.SourceToTarget;
			}

			public static DataAccessInterfaces.IRelationshipType AsRelationshipType(IObjectProperty self)
			{
				return null;
			}

			public static bool get_IsSymmetrical(IObjectProperty self)
			{
				return self.ForwardName == self.InverseName && self.SourceType.Equals(self.TargetType);
			}

			public static bool Equals(IEmittedPropertyAttribute self, DataAccessInterfaces.IConnectorType other)
			{
				return self.Equals(other);
			}
		}

		internal class NoRelationshipOrCompressedConnector : DataAccessInterfaces.IConnector
		{
			public NoRelationshipOrCompressedConnector(INode source, INode target, IObjectProperty referenceProp)
			{
				Source = source;
				Target = target;
				ConnectorType = referenceProp;
				Connectors = null;
			}

			public NoRelationshipOrCompressedConnector(INode source, INode target, IEnumerable<DataAccessInterfaces.IConnector> connectors)
			{
				Source = source;
				Target = target;
				ConnectorType = null;
				Connectors = connectors.ToArray();
			}

			public bool Equals(DataAccessInterfaces.IConnector other)
			{
				return Source.Equals(other.Source)
					&& Target.Equals(other.Target)
					&& this.ConnectorType != null
					? this.ConnectorType.Equals(other.ConnectorType)
						: other.Connectors != null && this.Connectors.SequenceEqual(other.Connectors);
			}

			public DataAccessInterfaces.INode Source { get; private set; }
			public DataAccessInterfaces.INode Target { get; private set; }
			public DataAccessInterfaces.IConnectorType ConnectorType { get; private set; }
			public IEnumerable<DataAccessInterfaces.IConnector> Connectors { get; private set; }

			public DataAccessInterfaces.IRelationship AsRelationship()
			{
				return null;
			}
		}

		// Note: core binding interfaces define IEntity as the most base class capable to represent a node
		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEntity")]
		public interface INode : IObject, DataAccessInterfaces.INode
		{
			[ExtensionProperty(typeof(NodeImpl))]
			new DataAccessInterfaces.INodeType Type { get; }

			// (KC) These properties supply (STATIC) TYPE info, so as can be seen in test usage above, 
			// would be used to form response in GetDataSetTypes()
			[ExtensionProperty(typeof(NodeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnectorType> ForwardExpandableConnectors { get; }
			[ExtensionProperty(typeof(NodeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnectorType> InverseExpandableConnectors { get; }

			// (KC) These properties supply INSTANCE info, to supply responses to existing calls like
			// GetRelationshipEntities(WithInstances!), GetEventParticipants
			[ExtensionMethod(typeof(NodeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnector> ForwardExpand(DataAccessInterfaces.IConnectorType connector);
			[ExtensionMethod(typeof(NodeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnector> InverseExpand(DataAccessInterfaces.IConnectorType connector);
		}

		public static class NodeImpl
		{
			public static DataAccessInterfaces.INodeType get_Type(INode self)
			{
				return self.GetType().GetAttribute<INodeType>();
			}

			public static IEnumerable<DataAccessInterfaces.IConnectorType> get_ForwardExpandableConnectors(INode self)
			{
				// KC. As per Impl on INodeType get_ForwardExpandableConnectors
				var referenceProps = self.Type.ReferenceProperties
					.Select(i => (DataAccessInterfaces.IConnectorType) i)
					.ToArray();

				var tempTypes = typeof (IRelationship).GetTypesAssignableFrom(false);
				var attribs = tempTypes.Select(i => i.GetProperty("Source"));

				var temp = self.GetReferringProperties(attribs);

				var outcomingRelationships = temp
					.Select(i => i.DeclaringType.GetAttribute<DataAccessInterfaces.IConnectorType>()) // declaring type is Ownership (
					.ToArray();

				return referenceProps.Union(outcomingRelationships);
			}

			public static IEnumerable<DataAccessInterfaces.IConnectorType> get_InverseExpandableConnectors(INode self)
			{
				// KC. As per Impl on INodeType get_InverseExpandableConnectors
				var nodeReferencePropInfos = typeof (INode).GetTypesAssignableFrom(false)
					.SelectMany(i => i.GetAttribute<INodeType>().ReferenceProperties)
					.Select(p => ((IObjectProperty) p).Compile());

				var inverseReferenceProps = self.GetReferringProperties(nodeReferencePropInfos)
					.Select(i => i.GetAttribute<DataAccessInterfaces.IConnectorType>())
					.ToArray();

				var incomingRelationships = self.GetReferringProperties(
					typeof(IRelationship).GetTypesAssignableFrom(false).Select(i => i.GetProperty("Target")))
					.Select(i => i.DeclaringType.GetAttribute<DataAccessInterfaces.IConnectorType>())
					.ToArray();

				return inverseReferenceProps.Union(incomingRelationships);
			}

			// KC. Note. Return type here is IConnectorType. To get current behaviour
			// for GetRelationshipEntities we would need to do this but also
			// enumerate "target" entities (only in case of IRelatonship). Implicit will
			// always return connector which contains target IEmittedInstance
			public static IEnumerable<DataAccessInterfaces.IConnector> ForwardExpand(
				INode self,
				DataAccessInterfaces.IConnectorType connectorType)
			{
				// KC. Implicit. Relationship would fail this because connectorType is IRelationshipType
				var refProp = connectorType as IObjectProperty;
				if (refProp != null)
				{
					// Implicit property type must be one of IEmittedInstance or IEnumerable<IEmittedInstance>
					// Return IConnector form NoRelationshipOrCompressedConnector
					var result = self.GetPropertyValue(refProp);
					if (result is IEmittedInstance)
					{
						return new [] { new NoRelationshipOrCompressedConnector(self, (INode)result, refProp) };
					}
					if (result is IEnumerable<IEmittedInstance>)
					{
						return (result as IEnumerable<IEmittedInstance>).Select(
							i => new NoRelationshipOrCompressedConnector(self, (INode) i, refProp));
					}
					throw new AssertionException("Unexpected type of reference property");
				}

				// Return the relationships pointing at the supplied instance
				var relationship = connectorType as IRelationshipType;
				if (relationship != null)
				{
					return self.GetReferringInstances(relationship.Compile().GetProperty("Source"))
						.Select(i => (IRelationship)i);
				}

				throw new AssertionException("Unexpected implemnetation of connector type");
			}

			public static IEnumerable<DataAccessInterfaces.IConnector> InverseExpand(
				INode self,
				DataAccessInterfaces.IConnectorType connectorType)
			{
				var refProp = connectorType as IObjectProperty;
				if (refProp != null)
				{
					return self.GetReferringInstances(refProp.Compile())
						.Select(i => new NoRelationshipOrCompressedConnector((INode) i, self, refProp));
				}

				var relationship = connectorType as IRelationshipType;
				if (relationship != null)
				{
					return self.GetReferringInstances(relationship.Compile().GetProperty("Target"))
						.Select(i => (IRelationship)i);
				}

				throw new AssertionException("Unexpected implemnetation of connector type");
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEntity")]
		public interface INodeType : IObjectType, DataAccessInterfaces.INodeType
		{
			[ExtensionProperty(typeof(NodeTypeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnectorType> ForwardExpandableConnectors { get; }
			[ExtensionProperty(typeof(NodeTypeImpl))]
			new IEnumerable<DataAccessInterfaces.IConnectorType> InverseExpandableConnectors { get; }

			// KC. No usage?
			[ExtensionMethod(typeof(NodeTypeImpl))]
			new DataAccessInterfaces.IConnectorType GetConnector(string moniker);
		}

		public static class NodeTypeImpl
		{
			public static IEnumerable<DataAccessInterfaces.IConnectorType> get_ForwardExpandableConnectors(
				INodeType self)
			{
				// (KC) Implicit. Get *ANY* property on the supplied type 
				// where is IEmittedInstance or IEnumerable<IEmittedInstance> 
				var referenceProps = self.ReferenceProperties
					// KC. We can cast because IObjectPropertyType implements IConnector!
					// this is safe for Reference properties because they are all garaunteed to be IObjectProperty  
					.Select(i => (DataAccessInterfaces.IConnectorType) i)
					.ToArray();

				// (KC) Get *ANY* relationship type where the source type
				// points to the supplied type (self)
				var relationships = typeof (IRelationship).GetTypesAssignableFrom(false)
					.Where(i => i.GetProperty("Source").PropertyType.IsAssignableFrom(self.Compile()))
					.Select(i => i.GetAttribute<IRelationshipType>())
					.ToArray();

				return referenceProps.Union(relationships);
			}

			public static IEnumerable<DataAccessInterfaces.IConnectorType> get_InverseExpandableConnectors(INodeType self)
			{
				// KC Implicit. Get *ALL* reference properties on *ANY* type where the reference type
				// matches the supplied type 
				var referenceProps = typeof (INode).GetTypesAssignableFrom(false)
					.SelectMany(i => i.GetAttribute<INodeType>().ReferenceProperties)
					.Select(p => ((IObjectProperty) p).Compile())
					.Where(p => p.PropertyType.IsAssignableFrom(self.Compile()) ||
						p.PropertyType.IsAssignableFrom(typeof(IEnumerable<>).MakeGenericType(self.Compile())))
					.Select(p => p.GetAttribute<DataAccessInterfaces.IConnectorType>())
					.ToArray();

				// (KC) Get *ANY* relationship type where the target type
				// points to the supplied type (self)
				var relationships = typeof(IRelationship).GetTypesAssignableFrom(false)
					.Where(i => i.GetProperty("Target").PropertyType.IsAssignableFrom(self.Compile()))
					.Select(i => i.GetAttribute<IRelationshipType>())
					.ToArray();

				return referenceProps.Union(relationships);
			}

			public static DataAccessInterfaces.IConnectorType GetConnector(INodeType self, string moniker)
			{
				var split = moniker.Split(':');
				if (split.Length == 1)
				{
					return typeof (IRelationship).GetTypesAssignableFrom(false)
						.First(i => i.GetId().ToString() == split[0]).GetAttribute<IRelationshipType>();
				}
				if (split.Length == 2)
				{
					return typeof (INode).GetTypesAssignableFrom(false)
						.First(i => i.GetId().ToString() == split[0])
						.GetProperties()
						.First(p => p.GetId().ToString() == split[1])
						.GetAttribute<IObjectProperty>();
				}
				throw new AssertionException("Client provided corrupted moniker");
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IRelationship")]
		public interface IRelationship : IObject, DataAccessInterfaces.IRelationship // KC, Is IConnector, IObject
		{
			[ExtensionProperty("Source")]
			IEntity NativeSource { get; }
			[ExtensionProperty("Target")]
			IEntity NativeTarget { get; }

			[ExtensionProperty(typeof(RelationshipImpl))]
			new DataAccessInterfaces.IRelationshipType Type { get; }

			[ExtensionProperty(typeof(RelationshipImpl))]
			new DataAccessInterfaces.IConnectorType ConnectorType { get; }

			[ExtensionProperty(typeof(RelationshipImpl))]
			new DataAccessInterfaces.INode Source { get; }
			[ExtensionProperty(typeof(RelationshipImpl))]
			new DataAccessInterfaces.INode Target { get; }

			[ExtensionProperty(typeof(RelationshipImpl))]
			new IEnumerable<DataAccessInterfaces.IConnector> Connectors { get; }

			[ExtensionMethod(typeof(RelationshipImpl))]
			new DataAccessInterfaces.IRelationship AsRelationship();

			[ExtensionMethod(typeof(RelationshipImpl))]
			new bool Equals(DataAccessInterfaces.IConnector other);
		}

		public static class RelationshipImpl
		{
			public static DataAccessInterfaces.IRelationshipType get_Type(IRelationship self)
			{
				return self.GetType().GetAttribute<IRelationshipType>();
			}

			public static DataAccessInterfaces.IConnectorType get_ConnectorType(IRelationship self)
			{
				// Type subclasses IConnector
				return self.Type;
			}

			public static DataAccessInterfaces.INode get_Source(IRelationship self)
			{
				return self.NativeSource;
			}

			public static DataAccessInterfaces.INode get_Target(IRelationship self)
			{
				return self.NativeTarget;
			}

			public static IEnumerable<DataAccessInterfaces.IConnector> get_Connectors(IRelationship self)
			{
				// ?
				return null;
			}

			public static DataAccessInterfaces.IRelationship AsRelationship(IRelationship self)
			{
				// effectively just a cast for working with the DataAccess interface? 
				return self;
			}

			public static bool Equals(IEmittedInstance self, DataAccessInterfaces.IConnector other)
			{
				return self.Equals(other);
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IRelationship")]
		public interface IRelationshipType : IObjectType, DataAccessInterfaces.IRelationshipType // KC IConnectorType, IObjectType
		{
			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new string Moniker { get; }

			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new DataAccessInterfaces.INodeType SourceType { get; }
			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new DataAccessInterfaces.INodeType TargetType { get; }

			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new string ForwardName { get; }
			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new string InverseName { get; }

			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new string ForwardDescription { get; }
			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new string InverseDescription { get; }

			[ExtensionProperty("Direction")]
			uint NativeDirectionality { get; set; }
			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new TestConnectorSuite.Directionality Directionality { get; set; }

			[ExtensionMethod(typeof(RelationshipTypeImpl))]
			new DataAccessInterfaces.IRelationshipType AsRelationshipType();

			[ExtensionProperty(typeof(RelationshipTypeImpl))]
			new bool IsSymmetrical { get; }

			[ExtensionMethod(typeof(RelationshipTypeImpl))]
			new bool Equals(DataAccessInterfaces.IConnectorType other);
		}

		public static class RelationshipTypeImpl
		{
			public static string get_Moniker(IRelationshipType self)
			{
				return self.Id.ToString();
			}

			public static DataAccessInterfaces.INodeType get_SourceType(IRelationshipType self)
			{
				return self.Compile().GetProperty("Source").PropertyType.GetAttribute<INodeType>();
			}

			public static DataAccessInterfaces.INodeType get_TargetType(IRelationshipType self)
			{
				return self.Compile().GetProperty("Target").PropertyType.GetAttribute<INodeType>();
			}

			public static string get_ForwardName(IRelationshipType self)
			{                
				return self.Compile().GetProperty("Source").GetAttribute<IEmittedPropertyAttribute>().InverseName;
			}

			public static string get_InverseName(IRelationshipType self)
			{
				return self.Compile().GetProperty("Target").GetAttribute<IEmittedPropertyAttribute>().InverseName;
			}

			public static string get_ForwardDescription(IRelationshipType self)
			{
				return self.Compile().GetProperty("Source").GetDescription();
			}

			public static string get_InverseDescription(IRelationshipType self)
			{
				return self.Compile().GetProperty("Target").GetDescription();
			}

			public static TestConnectorSuite.Directionality get_Directionality(IRelationshipType self)
			{
				return (TestConnectorSuite.Directionality)self.NativeDirectionality;
			}

			public static void set_Directionality(IRelationshipType self, TestConnectorSuite.Directionality value)
			{
				self.NativeDirectionality = (uint)value;
			}

			public static DataAccessInterfaces.IRelationshipType AsRelationshipType(IRelationshipType self)
			{
				return self;
			}

			public static bool get_IsSymmetrical(IRelationshipType self)
			{
				return self.ForwardName == self.InverseName && self.SourceType.Equals(self.TargetType);
			}

			public static bool Equals(IEmittedTypeAttribute self, DataAccessInterfaces.IConnectorType other)
			{
				return self.Equals(other);
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEntity")]
		public interface IEntity : INode, DataAccessInterfaces.IEntity
		{
			[ExtensionProperty(typeof(EntityImpl))]
			new DataAccessInterfaces.IEntityType Type { get; }
		}

		public static class EntityImpl
		{
			public static DataAccessInterfaces.IEntityType get_Type(IEntity self)
			{
				return self.GetType().GetAttribute<IEntityType>();
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEntity")]
		public interface IEntityType : INodeType, DataAccessInterfaces.IEntityType
		{
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEvent")]
		public interface IEvent : INode, DataAccessInterfaces.IEvent
		{
			[ExtensionProperty(typeof(EventImpl))]
			new DataAccessInterfaces.IEventType Type { get; }
		}

		public static class EventImpl
		{
			public static DataAccessInterfaces.IEventType get_Type(IEvent self)
			{
				return self.GetType().GetAttribute<IEventType>();
			}
		}

		[RequiresIBehaviour(TestConnectorSuite.Namespace + "IEvent")]
		public interface IEventType : INodeType, DataAccessInterfaces.IEventType
		{
		}
	}

	namespace TestConnector.DataAccessInterfaces
	{
		public interface IObject
		{
			ulong Id { get; }

			IObjectType Type { get; }

			Object GetPropertyValue(IObjectProperty property);
			void SetPropertyValue(IObjectProperty property, Object value);
		}

		public interface IObjectType
		{
			ulong Id { get; }
			string Name { get; }
			string Description { get; }

			// includes properties inherited from parent types
			IEnumerable<IObjectProperty> PrimitiveProperties { get; }
			IEnumerable<IObjectProperty> ReferenceProperties { get; }
		}

		public interface IObjectProperty
		{
			ulong Id { get; }
			string Name { get; }
			string Description { get; }

			Type PropertyType { get; }
			IObjectType DeclaringType { get; }

			bool IsReference();
		}

		public interface INode : IObject
		{
			new INodeType Type { get; }

			IEnumerable<IConnectorType> ForwardExpandableConnectors { get; }
			IEnumerable<IConnectorType> InverseExpandableConnectors { get; }

			IEnumerable<IConnector> ForwardExpand(IConnectorType connector);
			IEnumerable<IConnector> InverseExpand(IConnectorType connector);
		}

		public interface INodeType : IObjectType
		{
			IEnumerable<IConnectorType> ForwardExpandableConnectors { get; }
			IEnumerable<IConnectorType> InverseExpandableConnectors { get; }

			// returns descriptor of a connector by it's unique alias
			IConnectorType GetConnector(string moniker);
		}

		public interface IConnector : IEquatable<IConnector>
		{
			INode Source { get; }
			INode Target { get; }

			// null in case of compressed result from path finder
			IConnectorType ConnectorType { get; }

			// not null in case of compressed result from path finder
			IEnumerable<IConnector> Connectors { get; }

			// not null only if a connector is a relationship
			// in this case, there is additional information
			// managable by Get/SetPropertyValue on IObject
			IRelationship AsRelationship();
		}

		public interface IConnectorType : IEquatable<IConnectorType>
		{
			// unique identifier of a connector type
			string Moniker { get; }

			INodeType SourceType { get; }
			INodeType TargetType { get; }

			string ForwardName { get; }
			string InverseName { get; }

			string ForwardDescription { get; }
			string InverseDescription { get; }

			// for arrow visualization purposes only
			TestConnectorSuite.Directionality Directionality { get; }

			// true if source and target play the same role
			// in this case the expand should happen in both ways
			bool IsSymmetrical { get; }

			// not null only if a connector type is a relationship type
			// in this case, there is additional metadata (properties)
			// accessible on IObjectType
			IRelationshipType AsRelationshipType();
		}

		public interface IRelationship : IConnector, IObject
		{
			new IRelationshipType Type { get; }
		}

		public interface IRelationshipType : IConnectorType, IObjectType
		{
		}

		public interface IEntity : INode
		{
			new IEntityType Type { get; }
		}

		public interface IEntityType : INodeType
		{
		}

		public interface IEvent : INode
		{
			new IEventType Type { get; }

			DateTimeOffset Time { get; }
		}

		public interface IEventType : INodeType
		{
		}
	}
}