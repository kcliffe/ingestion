﻿using TaskExec.Infrastructure;
using System.IO;
using System.Text;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Task;

namespace TaskExec.Tasks
{
	/// <summary>
	/// Split a single file into multiple
	/// </summary>
	public class FileSplitTaskExecutor : BaseTaskExecutor
	{
		public FileSplitTaskExecutor (Signal signal) : base(signal)
		{
		}

		public override void Execute(string executor, TaskDefinition taskDefinition) 
		{
			var def = taskDefinition as FileSplitTaskDefinition;
			var targetFileName = Path.GetFileNameWithoutExtension (def.SourcePath);
			var targetExt = Path.GetExtension (def.SourcePath);
			var targetFolder = Utils.GetRootedPath (def.TargetFolder) + "/";
			var fileCount = 0;
			var currentLineCount = 0;
	
			if (!Directory.Exists (targetFolder)) {
				Directory.CreateDirectory (targetFolder);
			}

			using(var sr = File.OpenText(Utils.GetRootedPath(def.SourcePath))) {
				while (!sr.EndOfStream) {
					var sb = new StringBuilder ();
					while (currentLineCount++ < def.LinesPerFile && !sr.EndOfStream) {
						sb.AppendLine (sr.ReadLine ());
					}

					currentLineCount = 0;

					var path = Path.Combine (targetFolder, string.Format ("{0}_{1}{2}", 
						targetFileName, fileCount++, targetExt));

					File.WriteAllText (path, sb.ToString ());					
				}
			}
		    
			Progress.Update(taskDefinition.Id, taskDefinition.Name);
		}
	}
}

