﻿using System;
using System.IO;
using System.Linq;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Task;
using TaskExec.Infrastructure.Transport;

namespace TaskExec.Tasks
{
    /// <summary>
    /// Take a set of files in a folder (each containing a set of relationships)
    /// and for each queue a task to create relationships. 
    /// </summary>
    public class InitialiseRelationshipCreationTaskExecutor : BaseTaskExecutor
    {
        private readonly IProducer _publisher;

        public InitialiseRelationshipCreationTaskExecutor(Signal signal)
            : base(signal)
        {
            _publisher =
                TransportFactory.GetPublisher(TaskExecConsts.DefaultHost, TaskExecConsts.QueueNames.Worker);
        }

        public override void Execute(string executorName, TaskDefinition taskDefinition)
        {
            var createRelationshipsTaskDefinition = taskDefinition as InitialiseRelationshipCreationTaskDefinition;
            var path = Utils.GetRootedPath(createRelationshipsTaskDefinition.SourcePath);

            var files = Directory.EnumerateFiles(path).ToList();

            // read all files in directory matching mask
            // for each, dispatch sub task with originating 
            files.ForEach(file =>
            {
                var createRelationshipTasksDefinition = new CreateRelationshipsTaskDefinition
                {
                    Id = Guid.NewGuid().ToString(),
                    OriginatingId = taskDefinition.Id,
                    SourcePath = file
                };

                _publisher.Send(createRelationshipTasksDefinition);
            });

			Progress.Update(taskDefinition.Id, taskDefinition.Name, files.Count,  TaskExecConsts.Params.InitialiseOnly);
        }
    }
}

