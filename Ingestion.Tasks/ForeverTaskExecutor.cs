﻿using System;
using System.Threading;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	/// <summary>
	/// For demoing cancellation
	/// </summary>
	public class ForeverTaskExecutor : BaseTaskInvoker
	{
		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
		public ForeverTaskExecutor (Signal signal) : base(signal)
		{
		}

		public override void Execute(string executor, TaskDefinition taskDefinition)
		{
			Logger.Info ("Forever task started at {0}", DateTime.Now);

			while (!Signal.IsSet) {
				Logger.Info ("Exec at {0}", DateTime.Now);
				Thread.Sleep (1000);
			}

			Logger.Info ("Task stopped at {0}", DateTime.Now);
		}
	}
}

