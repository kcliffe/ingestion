namespace TaskExec.Tasks
{
	public class Relationship
	{
		public string SourceId {
			get;
			set;
		}
		public string TargetId {
			get;
			set;
		}
	}

}

