﻿using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	/// <summary>
	/// Echo a string to the console
	/// </summary>
	public class EchoMessageExecutor : BaseTaskInvoker
	{
		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
        public EchoMessageExecutor(Signal signal)
            : base(signal)
		{			
		}

		public override void Execute(string executor, TaskDefinition taskDefinition)
		{
			// elasticLogger.Log(executor, "Processing task {0} ...", taskDefinition.Name);

		    var message = taskDefinition.GetString("Msg");        // ick. in most ways strongly typed was better    
			Logger.Info ("Echo: {0}", message);
			
			// note default WorkUnits value of 1 should indicate we've started (and finished :-0)
			Progress.Update(taskDefinition.Id, taskDefinition.Name);                         
		}
	}
}

