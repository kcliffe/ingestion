﻿using System;
using System.Threading;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Tasks
{
	/// <summary>
	/// Echo a string to the console
	/// </summary>
	public class RunForXSecondsExecutor : BaseTaskInvoker
	{
		/// <summary>
		/// Required for factory, dont remove
		/// </summary>
        public RunForXSecondsExecutor(Signal signal)
            : base(signal)
		{			
		}

		public override void Execute(string executor, TaskDefinition taskDefinition)
		{
            var inSeconds = taskDefinition.GetInt("Secs");        // ick. in most ways strongly typed was better    
			Logger.Info ("Running for: {0} secs", inSeconds);
			
            Thread.Sleep(TimeSpan.FromSeconds(inSeconds));

			// note default WorkUnits value of 1 should indicate we've started (and finished :-0)
			Progress.Update(taskDefinition.Id, taskDefinition.Name);                         
		}
	}
}

