﻿using System.IO;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Task;

namespace TaskExec.Tasks
{
	/// <summary>
	/// Crazy sample task to move a file from one location to another?
	/// </summary>
	public class PreProcessRelationshipsTaskExecutor : BaseTaskExecutor
	{
		public PreProcessRelationshipsTaskExecutor (Signal signal) : base(signal)
		{
		}

		public override void Execute (string executor, TaskDefinition taskDefinition)
		{
			var def = taskDefinition as PreProcessRelationshipsTaskDefinition;

			var source = Utils.GetRootedPath (def.SourcePath);
			var dest = Utils.GetRootedPath (def.TargetPath);
			if (File.Exists(dest)) {
				File.Delete(dest);
			}

			File.Copy (source, dest);

			// elasticLogger.Log (executor, "File copied from {0} to {1}", source, dest);

			Progress.Update(taskDefinition.Id, taskDefinition.Name);
		}
	}
}

