﻿using System.IO;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Task;

namespace TaskExec.Tasks
{
	/// <summary>
	/// Create relationships from a single file containing 1+ of them.
	/// </summary>
	public class CreateRelationshipsTaskExecutor : BaseTaskExecutor
	{
		public CreateRelationshipsTaskExecutor (Signal signal) : base(signal)
		{
  		}

		public override void Execute (string executorName, TaskDefinition taskDefinition)
		{
			var createRelationshipTasksDef = taskDefinition as CreateRelationshipsTaskDefinition;

			// for demo of execption handling
			//if (ct.ShouldThrowBecause())
			//	throw new Exception("Ouchy");

			var path = Utils.GetRootedPath(createRelationshipTasksDef.SourcePath);
			var serializer = new JsonSerializer ();
		    var checkpointEveryXRelationships = 5;  // could be on taskDef
		    var relCount = 0;
            
			using (var sr = File.OpenText (path)) {
				while (!sr.EndOfStream) {

					var relationship = serializer.Deserialize<Relationship> (sr.ReadLine ());
				    relCount++;

					Logger.Info("{2}: Creating relationship with source id {0} and target id {1}",
							relationship.SourceId,
							relationship.TargetId,
							executorName);

				    if (relCount%checkpointEveryXRelationships != 0) continue;

				    var restartInfo = new { RelationshipCount = relCount };
				    Progress.CreateCheckpoint(taskDefinition.Id, restartInfo);
				}
			}

			// the originating Id is the InitialiseRelationshipCreation task that created
			// this task. So the pattern is: <<child>> tasks use the <<originating id>>
			// to track progress against the <<parent>>
			Progress.Update(createRelationshipTasksDef.OriginatingId, createRelationshipTasksDef.Name);    
		}
	}
}

