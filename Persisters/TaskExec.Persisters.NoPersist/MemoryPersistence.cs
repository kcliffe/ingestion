﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TaskExec.Infrastructure.Persist;
using TaskExec.Infrastructure.Persist.Schema;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure;
using NLog;
using AutoMapper;

namespace TaskExec.Persisters.NoPersist
{
    public class MemoryPersistence : IPersistManifests
    {
		// this doubles as our cache :-) All persisters should employ write-thru cache.
        private readonly ConcurrentDictionary<string, IPersistableManifest> _manifests;
		private readonly ConcurrentDictionary<string, IPersistableTask> _tasks;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MemoryPersistence()
        {
            _manifests = new ConcurrentDictionary<string, IPersistableManifest>();
			_tasks = new ConcurrentDictionary<string, IPersistableTask> ();

			Mapper.CreateMap<IPersistableManifest, ManifestDefinition> ();
			Mapper.CreateMap<ManifestDefinition, IPersistableManifest> ();
			Mapper.CreateMap<IPersistableJob, JobDefinition> ();
			Mapper.CreateMap<JobDefinition, IPersistableJob> ();
			Mapper.CreateMap<IPersistableTask, TaskDefinition> ();
			Mapper.CreateMap<TaskDefinition, IPersistableTask> ();
        }

        public List<ManifestDefinition> GetAll()
		{
			try{
			return _manifests.Values.Select(m => 
				Mapper.Map<ManifestDefinition>(m)).ToList();
			}
			catch(Exception ex)
			{
				return null;
			}
        }

        public ManifestDefinition GetById(string id)
        {
            IPersistableManifest manifest;
            if (_manifests.TryGetValue(id, out manifest))            
                return Mapper.Map<ManifestDefinition>(manifest);

            throw new Exception(string.Format("Manifest {0} not found", id));
        }

        public ManifestDefinition Put(ManifestDefinition manifestDefinition)
        {
            if (string.IsNullOrEmpty(manifestDefinition.Id))
            {
				manifestDefinition.Id = string.Format("{0}_{1}",manifestDefinition.Id, DateTime.Now.ToFileTime());
            }

			var persistableManifest = Mapper.Map<IPersistableManifest>(manifestDefinition);
			if (!_manifests.ContainsKey (manifestDefinition.Id)) 
			{
				persistableManifest.Jobs
					.SelectMany (j => j.Tasks).ToList()
					.ForEach(t => _tasks.TryAdd(t.Id, t));
			}

            _manifests[manifestDefinition.Id] = persistableManifest;
            
            return manifestDefinition;
        }

		public void UpdateTask(TaskDefinition taskDefinition)
		{
			var persistableTask = _tasks[taskDefinition.Id];
			Mapper.Map (taskDefinition, persistableTask);
		}
    }
}