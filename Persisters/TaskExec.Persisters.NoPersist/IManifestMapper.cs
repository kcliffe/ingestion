﻿using TaskExec.Infrastructure.Persist.Schema;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Persisters.NoPersist
{
    public interface IManifestMapper
    {
        IPersistableManifest ToPersistableManifest(ManifestDefinition definition);
        ManifestDefinition ToManifestDefinition(IPersistableManifest persistableManifest);
    }
}