﻿using System.Linq;
using TaskExec.Infrastructure.Persist.Schema;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Persisters.NoPersist
{
	/// <summary>
	/// Translate between serializable and binding versions of manifests / jobs / tasks
	/// </summary>
    public class ManifestMapper : IManifestMapper
    {
        public IPersistableManifest ToPersistableManifest(ManifestDefinition definition)
        {
            var persistableManifest = new PersistableManifest
            {
				Id = definition.Id,               
                Jobs = definition.Jobs.Select(jDef => new PersistableJob
                {
                    Name = jDef.Name,
                    Description = jDef.Description, 
                    Tasks = jDef.Tasks.Select(tDef => new PersistableTask
                    {
						Id=tDef.Id,
                        Name = tDef.Name,
                        Description = tDef.Description, 
                        Depends = tDef.Depends,
                        ExecutionState = tDef.ExecutionState,
						JobName = tDef.JobName,
                        ManifestId = tDef.ManifestId,
                        OriginatingId = tDef.OriginatingId,
						Params = tDef.Params,
                        QueueUntil = tDef.QueueUntil,
                        Route = tDef.Route,
						Set = tDef.Set,
                        TotalWork = tDef.TotalWork,
						UnSet = tDef.UnSet,
                        WorkCompleted = tDef.WorkCompleted,
                    }).Cast<IPersistableTask>().ToList()
                }).Cast<IPersistableJob>().ToList()
            };

            return persistableManifest;
        }

        public ManifestDefinition ToManifestDefinition(IPersistableManifest persistableManifest)
        {
            var manifestDef = new ManifestDefinition
            {
				Id = persistableManifest.Id,            
                Jobs = persistableManifest.Jobs.Select(pJob => new JobDefinition
                {
                    Name = pJob.Name,
                    Description = pJob.Description,
                    Tasks = pJob.Tasks.Select(pTask => new TaskDefinition
                    {                        
                        Id = pTask.Id,
                        Description = pTask.Description,
                        DependsOn = string.Join(",", pTask.Depends),
                        ExecutionState = pTask.ExecutionState,
                        JobName = pTask.JobName,
                        ManifestId = pTask.ManifestId,
						Name = pTask.Name,
                        OriginatingId = pTask.OriginatingId,
                        QueueUntil = pTask.QueueUntil,
						Params = pTask.Params,
                        Route = pTask.Route,
						Set = pTask.Set,
                        TotalWork = pTask.TotalWork,
						UnSet = pTask.UnSet,
                        WorkCompleted = pTask.WorkCompleted
                    }).ToList()
                }).ToList()
            };

            return manifestDef;
        }
    }
}