 public interface ITaskContext
    {
        UInt64 TaskId { get; }
        UInt64 JobId { get; }
        Int32 JobNumber { get; }
        Int32 TaskNumber { get; }
        /// <summary>
        /// Get the ID of the group initializer task. A unique ID for a parallel task group
        /// </summary>
        UInt64 GroupId { get; }

        string TaskName { get; }
        string Description { get; }
        string TaskIdentifierString { get; }

        bool IsIncremental { get; }
        bool IsMultiple { get; }
        bool IsExternal { get; }

        bool IsRestarted { get; }
        String GetRestartInfo();
        void SaveRestartInfo(String data);

        bool IsCancelled { get; }
        List<Exception> Exceptions { get; }

        void ThrowIfCancellationRequested();
        
        void UpdateProgress(String message, long percentage);

        string GetParallelGroupData();
        void SetParallelGroupData(string parallelGroupData);
        void AppendParallelGroupData(string parallelGroupData);

        void SetSharedGroupObject(object state);
        object GetSharedGroupObject();

        string ApplicationBase { get; }
        string DataSetName { get; }
        TimeSpan BatchCommitInterval { get; }
        string DocumentDirectory { get; }
        string InputDirectory { get; }
        string OutputDirectory { get; }
        string FileName { get; }
        int Partitions { get; }
        int PartitionId { get; }
        PartitioningStyle Partitioning { get; }
        int ScoresDocumentThreshold { get; }
        string EntityTypeName { get; }

        string GetStringValue(string name, string defaultValue = null);
        int GetIntegerValue(string name, int? defaultValue = null);
        bool GetBooleanValue(string name, bool? defaultValue = null);
        double GetDoubleValue(string name, double? defaultValue = null);
        TimeSpan GetTimeSpanValue(string name, TimeSpan? defaultValue = null);
        bool IsParameterDefined(string name);
        DirectoryInfo GetDirectoryValue(string name, string baseDirectory = null);
        FileInfo GetFileValue(string name, string baseDirectory = null, string defaultValue = null);
        bool GetLongParallelTaskDataValue(string variableName, out long value);
        void SetLongParallelTaskDataValue(string variableName, long value);
        void AppendLongParallelTaskDataValue(string variableName, long value);
        IEnumerable<string> GetParameterNames();
    }