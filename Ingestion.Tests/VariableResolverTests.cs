﻿using System;
using NUnit.Framework;
using TaskExec.Conductor.Config;

namespace Ingestion.Tests
{
	[TestFixture]
	public class WildcardResolverTests
	{
		[Test]
		public void SpecialFolder_resolved_if_folder_valid()
		{
			string outstr;
			var handled = new SpecialFolderVariableResolver().Handle ("specialFolder.ApplicationData", out outstr);

			Assert.That(true, Is.EqualTo(handled));
		}

		[Test]
		public void EnvironmentVariable_resolved_if_param_valid()
		{
			string outstr;
			var handled = new EnvironmentVariableVariableResolver().Handle ("envvar.Bob", out outstr);

			Assert.That(true, Is.EqualTo(handled));
		}
	}

	class TestResolver : BaseVariableResolver
	{
		public override bool Handle (string wildcard, out string val) // todo. mod wildCard in place instead?
		{
			val = string.Empty;
			if (!base.IsMatch (wildcard, "test"))
				return false;

			val = "ok";
			return true;
		}
	}
}

