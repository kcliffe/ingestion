﻿using System;
using NUnit.Framework;
using TaskExec.Conductor.Config;
using System.Collections.Generic;
using TaskExec.Infrastructure;

namespace Ingestion.Tests
{
	[TestFixture]
	public class VariableResolverTests
	{
		[Test]
		public void CanResolveValidSpecialFolder ()
		{
			ConfigurationProcessor cp = new ConfigurationProcessor (
				new List<IVariableResolver> 
				{
					new SpecialFolderVariableResolver()
				});

			cp.ResolveVariableExpression(string.Format("{0}.{1}", Consts.VariablePrefixes.SpecialFolder, "MyDocuments"));
		}

		[Test]
		[ExpectedException(typeof(FormatException))]
		public void InvalidSpecialFolderThrows ()
		{
			ConfigurationProcessor cp = new ConfigurationProcessor (
				new List<IVariableResolver> 
				{
					new SpecialFolderVariableResolver()
				});

			cp.ResolveVariableExpression(string.Format("{0}.{1}", Consts.VariablePrefixes.SpecialFolder, "bad!"));
		}

		[Test]
		public void CanResolveValidEnvVar ()
		{
			ConfigurationProcessor cp = new ConfigurationProcessor (
				new List<IVariableResolver> 
				{
					new SpecialFolderVariableResolver(),
					new EnvironmentVariableVariableResolver()
				});

			cp.ResolveVariableExpression(string.Format("{0}.{1}", Consts.VariablePrefixes.EnvironmentVariable, "PATH"));
		}

		[Test]
		[ExpectedException(typeof(FormatException))]
		public void InvalidEnvVarThrows ()
		{
			ConfigurationProcessor cp = new ConfigurationProcessor (
				new List<IVariableResolver> 
				{
					new SpecialFolderVariableResolver(),
					new EnvironmentVariableVariableResolver()
				});

			cp.ResolveVariableExpression(string.Format("{0}.{1}", Consts.VariablePrefixes.EnvironmentVariable, "bad!"));
		}
	}
}

