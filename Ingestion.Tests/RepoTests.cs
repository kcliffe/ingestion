﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using TaskExec.Conductor.Management;
using TaskExec.Infrastructure.Persist;
using TaskExec.Infrastructure.Serialize;

namespace Ingestion.Tests
{
	[TestFixture]
	public class Test
	{
        IPersistManifests mockRepo;
	    private IFileSystem mockFileSystem;

		[Test]
		public void Get_pending_tasks_returns_all_pending_tasks ()
		{
		    mockRepo = new Moq.Mock<IPersistManifests>().Object;
		    
			var repo = new TaskManagement (mockRepo, mockFileSystem);
			var pending = repo.GetPendingTasks ();
			Assert.That(4, Is.EqualTo(pending.Count()));
		}

		[Test]
		public void Completed_task()
		{
            var repo = new TaskManagement (mockRepo, mockFileSystem);
			var pending = repo.GetPendingTasks ();

			repo.SetTaskComplete (pending.First().Id);
			Assert.That(3, Is.EqualTo(repo.GetPendingTasks ().Count()));
		}

		[Test]
		public void Executing_task()
		{
            var repo = new TaskManagement (mockRepo, mockFileSystem);
			var pending = repo.GetPendingTasks ();

			repo.SetTaskExecuting (pending.First().Id);
			Assert.That(3, Is.EqualTo(repo.GetPendingTasks ().Count()));
		}

		[Test]
		public void Executing_jobs()
		{
            var repo = new TaskManagement (mockRepo, mockFileSystem);
			var pending = repo.GetPendingTasks ();

			repo.SetTaskExecuting (pending.First().Id);
			repo.SetTaskExecuting (pending.Skip(2).First().Id);
			Assert.That(2, Is.EqualTo(repo.QueryExecutingJobs ().Count()));
		}

		[Test]
		public void Executing_tasks_for_job()
		{
            var repo = new TaskManagement (mockRepo, mockFileSystem);
			var pending = repo.GetPendingTasks ();

			repo.SetTaskExecuting (pending.First().Id);
			repo.SetTaskExecuting (pending.Skip(1).First().Id);
			Assert.That(2, Is.EqualTo(repo.QueryExecutingTasksForJob (pending.First().JobName).Count()));
			Assert.That(0, Is.EqualTo(repo.QueryExecutingTasksForJob (pending.Skip(3).First().JobName).Count()));
		}

        private MockFileSystem SetupFileSystem()
        {
            var manifest = new ManifestDefinition()
            {
                Id = "m_x",
                Jobs = new List<JobDefinition> {
                    new JobDefinition {
                        Name = "J1",
                        Tasks = new List<TaskDefinition> {
                            new FakeTask {
                                Name = "J1+T1"
                            },
                            new FakeTask {
                                Name = "J1+T2"
                            }
                        }
                    },
                    new JobDefinition {
                        Name = "J2",
                        Tasks = new List<TaskDefinition> {
                            new FakeTask {
                                Name = "J2+T1"
                            },
                            new FakeTask {
                                Name = "J2+T2"
                            }
                        }
                    }
                }
            };

            var manifestStream = new JsonSerializer().Serialize<ManifestDefinition>(manifest);
            var s2 = System.Text.Encoding.ASCII.GetString(manifestStream);

            return new MockFileSystem(new Dictionary<string, MockFileData>
                {
                    { @"/home/kelly/localdev/Ingestion/data/current/manifest.json", 
                        new MockFileData(s2) },
                });
        }
	}

	public class FakeTask : TaskDefinition
	{
	  
	}
}

