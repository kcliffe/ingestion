namespace TaskExec.Infrastructure.Management
{
	public class WorkerRegistration : NotificationBase
	{
		private readonly string _name;
		private readonly string _hostName;
		private readonly string _inputQueue;

		public string Name { get { return _name; } }
		public string HostName { get { return _hostName; }}
		public string InputQueue { get { return _inputQueue; }}

		public WorkerRegistration (string name, string hostName, string inputQueue)
		{
			_name = name;
			_hostName = hostName;
			_inputQueue = inputQueue;
		}

		public override string ToString ()
		{
			return string.Format ("Name={0}, HostName={1}, InputQueue={2}", _name, _hostName, _inputQueue);
		}
	}

}
