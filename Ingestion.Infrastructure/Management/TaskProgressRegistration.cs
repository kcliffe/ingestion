using System;
using System.Collections.Generic;

namespace Ingestion.Infrastructure
{
	/// <summary>
	/// Task progress registration is sent when task execution is started
	/// </summary>
	public class TaskProgressRegistration : NotificationBase
	{
		public string TaskName { get; set; }

		/// <summary>
		/// If the task is broken into worker tasks, this is the total count
		/// which allows us to calculate progress
		/// </summary>
		/// <value>The work units.</value>
		private int workUnits = 1;
		public int WorkUnits 
		{ 
			get { return workUnits; }	
			set { workUnits = value; } 
		}
	}
}

