﻿using TaskExec.Infrastructure.Transport;

namespace TaskExec.Infrastructure.Management
{
	public class SignalNotificationBase : NotificationBase, IHasMessage
	{
		//public Signal.SignalType SignalType { get; set; }
		public bool AllWorkers { get; set; }
		public string Message { get; set; } // we don't currently utilise the message?
	}

    public class CancelSignalNotification : SignalNotificationBase
    {
		public string ManifestName {get; private set;}

		public CancelSignalNotification (string manifestName)
		{
			ManifestName = manifestName;
		}
    }

    public class AbortSignalNotification : SignalNotificationBase
    {      
		public string ManifestName {get; private set;}

		public AbortSignalNotification (string manifestName)
		{
			ManifestName = manifestName;
		}
    }

    public class RestartSignalNotification : SignalNotificationBase
    {
		public string ManifestName {get; private set;}

		public RestartSignalNotification (string manifestName)
		{
			ManifestName = manifestName;
		}
    }
}

