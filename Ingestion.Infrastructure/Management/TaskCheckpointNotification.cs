﻿using TaskExec.Infrastructure;

namespace TaskExec.Infrastructure.Management
{
    public class TaskCheckpointNotification : NotificationBase
    {
        private readonly string restartInfoValue;
        public string RestartInfoValue
        {
            get { return restartInfoValue; }
        }

        public TaskCheckpointNotification(string taskId, string restartInfoValue)
        {
            TaskId = taskId;
            this.restartInfoValue = restartInfoValue;
        }
    }
}
