﻿using TaskExec.Infrastructure;

namespace TaskExec.Infrastructure.Management
{
	/// <summary>
	/// Send when a unit of work that is part of a task completes
	/// </summary>
	public class TaskProgressUpdate : NotificationBase
	{       
		/// <summary>
		/// If the task is broken into worker tasks, this is the total count
		/// which allows us to calculate progress
		/// </summary>
		/// <value>The work units.</value>
		private int workUnits = 1;
		public int WorkUnits 
		{ 
			get { return workUnits; }	
			set { workUnits = value; } 
		}

		private bool initialiseOnly;
		public bool InitialiseOnly 
		{ 
			get { return initialiseOnly; }	
			set { initialiseOnly = value; } 
		}
	}
}

