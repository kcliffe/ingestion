﻿namespace TaskExec.Infrastructure.Management
{
	public class NewManifestNotification : NotificationBase
	{
		public string ManifestPath { get; set; }
	}
}

