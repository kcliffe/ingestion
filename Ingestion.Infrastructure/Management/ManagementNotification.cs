﻿using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Transport;
using NLog;
using TaskExec.Infrastructure.Persist;

namespace TaskExec.Infrastructure.Management
{
	/// <summary>
	/// Sends progress notifications
	/// </summary>
    public class ManagementNotification
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // If IOC then not static, use Static lifetime in container
		private static IProducer notificationPublisher;

        public ManagementNotification()
        {
			notificationPublisher = 
				TransportFactory.GetPublisher(Consts.DefaultHost, 
					Consts.QueueNames.Conductor);            
        }
	
		public void Update(string taskId, string taskName, int workUnits = 1, bool initialiseOnly = false)
		{
			notificationPublisher.Send(new TaskProgressUpdate
            {
					TaskId = taskId,
					WorkUnits = workUnits,
					InitialiseOnly = initialiseOnly
            });

			Logger.Debug("Updating the progress of task {0}", taskName);
        }

		public void RegisterWorker(WorkerRegistration workerRegistration)
		{
			notificationPublisher.Send (workerRegistration);
			Logger.Debug("Registering worker {0}", workerRegistration);
		}

        /// <summary>
        /// Creating a checkpoint which allows individual tasks to persist some sementically meaningful
        /// representation of their state which can be used in restart scenario's. 
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="restartInfoValue"></param>
	    public void CreateCheckpoint(string taskId, dynamic restartInfoValue)
	    {
            var wrappedRestartInfo = new RestartInfoWrapper().Wrap(restartInfoValue);
            notificationPublisher.Send(new TaskCheckpointNotification(taskId, wrappedRestartInfo));
            Logger.Debug("Createing checkout for task {0}", taskId);
	    }
    }    
}
