﻿using System;

namespace TaskExec.Infrastructure
{
	public class NonTransientException : Exception
	{
		public NonTransientException (string message) : base(message)
		{
		}

		public NonTransientException (string message, Exception ex) : base(message, ex)
		{
		}
	}

	public class TaskExecutorResolverException : NonTransientException
	{
		public TaskExecutorResolverException (string message) : base(message)
		{
		}

		public TaskExecutorResolverException (string message, Exception ex) : base(message, ex)
		{
		}
	}
}

