using System;
using System.Collections.Generic;
using System.Linq;

// can be moved now that we have persistent classes
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Persist
{
	/// <summary>
	/// A manifest is a simple list of jobs (json formatted).
	/// 
	/// Sorting currently only works if jobs / tasks specifying dependencies
	/// are in ascending order in the manifest
	/// </summary>
	public class Manifest
	{
		public string Name {get;set;}
		public List<JobDefinition> Jobs { get;	set; }
	
		// KC. Not required i guess once we start actually persisting manifests / jobs / tasks
		public void AssignUniqueIdToSubTasks ()
		{
			Jobs.SelectMany (j => j.Tasks).ToList().ForEach (t=> t.Id = Guid.NewGuid().ToString());
		}

		public void CopyManifestNameToSubTasks ()
		{
			Jobs.ForEach (j => j.Tasks.ForEach (t=> t.ManifestName = Name));
		}

		public void CopyJobNameToSubTasks ()
		{
			Jobs.ForEach (j => j.Tasks.ForEach (t=>t.JobName = j.Name));
		}
				
		public void MergeJobLevelDependenciesToSubTasks ()
		{
			foreach (var j in Jobs) {
				if (j.HasDependecies) {
					foreach(var t in j.Tasks) {
						t.MergeDependencies(j.Depends);
					}
				}
			}
		}

		public void ResolveWildcardExpressions()
		{
			foreach (var j in Jobs) 
			{
				// do stuff with params that can be wild carded
				foreach(var t in j.Tasks) 
				{
					// and again...
				}
			}
		}
	}
}
