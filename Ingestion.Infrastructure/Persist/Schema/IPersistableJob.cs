using System.Collections.Generic;

namespace TaskExec.Infrastructure.Persist.Schema
{
    /// <summary>
    /// Persisted jobs is fairly anemic - most of the properties that can be specified on them via config are merged down to Task
    /// when read from Config
    /// </summary>
	public interface IPersistableJob
	{
		string Id { get; set; } // default to manifestId + unique index
		string Name { get; set; }
        string Description { get; set; }
		List<IPersistableTask> Tasks { get; set;}
	}
}

