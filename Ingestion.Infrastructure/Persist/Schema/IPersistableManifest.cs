using System.Collections.Generic;

namespace TaskExec.Infrastructure.Persist.Schema
{
	public interface IPersistableManifest
	{
        string Id { get; set; }
		string Name {get;set;}
		List<IPersistableJob> Jobs { get; set;}	    
	}
}

