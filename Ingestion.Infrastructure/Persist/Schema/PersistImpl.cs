﻿using System.Collections.Generic;

namespace TaskExec.Infrastructure.Persist.Schema
{
    /// <summary>
    /// These classes ONLY required for mapping Definition* classes to Persistentent
    /// </summary>
    public class PersistableManifest : IPersistableManifest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<IPersistableJob> Jobs { get; set; }
    }

    public class PersistableJob : IPersistableJob
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<IPersistableTask> Tasks { get; set; }
        /*
         *Sigh, will probably need to dup some fields from PeristableTask
         */
    }

    public class PersistableTask : IPersistableTask
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string JobId { get; set; }
        public string Route { get; set; }
        public string OriginatingId { get; set; }
        public string ManifestId { get; set; }
        public string JobName { get; set; }
        public int? TotalWork { get; set; }
        public double WorkCompleted { get; set; }                
        public ExecutionState ExecutionState { get; set; }        
        public List<string> Depends { get; set; }
        public List<string> QueueUntil { get; set; }
        public List<string> Set { get; set; }
        public List<string> UnSet { get; set; }
        public Dictionary<string, string> Params { get; set; }        
    }
}
