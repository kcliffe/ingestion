using System.Collections.Generic;

namespace TaskExec.Infrastructure.Persist.Schema
{
	public interface IPersistableTask
	{
		string Id { get; set;}
		string Name { get; set; }
        string Description { get; set; }
		string JobId { get; set; }
		string Route { get;set; }
		string OriginatingId { get;set; }
		ExecutionState ExecutionState { get; set; } // persist?
		string ManifestId { get; set; }
		string JobName { get; set; }
		int? TotalWork { get; set; }        
		double WorkCompleted { get; set;	}

        // these fields from the JobBase (TaskBase)
        List<string> Depends { get; set; }
        List<string> QueueUntil { get; set; }
        List<string> Set { get; set; }
        List<string> UnSet { get; set; }

        // see comments on TaskDefinition
        Dictionary<string, string> Params { get; set; }
	}
}

