﻿using System.Collections.Generic;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Persist
{
	public interface IPersistManifests
	{
	    List<ManifestDefinition> GetAll();
		ManifestDefinition GetById (string id); 
        ManifestDefinition Put(ManifestDefinition manifestDefinition);
		void UpdateTask (TaskDefinition taskDefinition);
	}
}

