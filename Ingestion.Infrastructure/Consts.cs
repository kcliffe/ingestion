﻿using System.Collections.Generic;

namespace TaskExec.Infrastructure
{
	public static class Consts
	{
		public const string DefaultHost = "localhost";
        public const string SystemBackChannelQueueName = "system_backchannel";

		public static class VariablePrefixes
		{
			public const string SpecialFolder = "specialfolder";
			public const string EnvironmentVariable = "envvar";
		}

		public static class QueueNames
		{
			public const string Worker = "worker_queue";
			public const string Conductor = "conductor_queue";
		}

		public static class Categories
		{
			public const string Echo = "Echo";
			public const string CreateRelationships = "CreateRelationships";
			public const string CreateRelationship = "CreateRelationship";
		}

		public static class Params
		{
            public static bool InitialiseOnly = true;
            public static string DefaultExchange = "";
            public static bool DurableQueue = true;
            public static bool NonDurableQueue = false;
            public static bool AutoDeleteOn = false;
            public static bool AutoDeleteOff = false;
            public static bool NonExclusiveQueue = false;
            public static IDictionary<string, object> NoArgs = null;
		    public static string NoRoutingKey = string.Empty;
		}
	}

    public enum ExecutionState
    {
        Pending,
		Cancelled,
		Aborted,
        Executing,
        Complete
    }
}

