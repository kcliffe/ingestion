using System;
using System.Collections.Generic;

namespace Ingestion.Infrastructure
{
	public class TaskDefinition
	{
		public string Id { get;set; }
		public string Name { get;set; }
	}
}
