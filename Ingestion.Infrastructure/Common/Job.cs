using System;
using System.Collections.Generic;

namespace Ingestion.Infrastructure
{
	public class Job
	{
		public List<TaskDefinition> Tasks { get;set; }
	}
}
