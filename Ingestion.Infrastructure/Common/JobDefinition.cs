using System;
using System.Collections.Generic;

namespace Ingestion.Infrastructure
{
	public class JobDefinition
	{
		public List<Job> Jobs { get;set; }
	}
}
