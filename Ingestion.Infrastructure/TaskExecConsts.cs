﻿namespace TaskExec.Infrastructure
{
	public static class TaskExecConsts
	{
		public const string DefaultHost = "localhost";

		public static class QueueNames
		{
			public const string Worker = "worker_queue";
			public const string Conductor = "conductor_queue";
		}

		public static class Categories
		{
			public const string Echo = "Echo";
			public const string CreateRelationships = "CreateRelationships";
			public const string CreateRelationship = "CreateRelationship";
		}

		public static class Params
		{
			public const bool InitialiseOnly = true;
            public const string DefaultExchange = "";
		}
	}    
}

