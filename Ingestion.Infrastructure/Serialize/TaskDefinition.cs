﻿using System;
using System.Linq;

namespace TaskExec.Infrastructure.Serialize
{
    public class TaskDefinition : JobBase
    {       
        public string Id { get; set; }
        public string Route { get; set; }	// currently only utilised for routing (queue name)        
        public string OriginatingId { get; set; }
        public ExecutionState ExecutionState { get; set; }
        public string ManifestId { get; set; }
        public string JobName { get; set; }
        public int? TotalWork { get; set; }
        public double WorkCompleted { get; set; }

        public bool HasRoute
        {
            get { return !string.IsNullOrWhiteSpace(Route); }
        }

        public override string ToString()
        {
            return string.Format("[TaskDefinition: Id={0}, OriginatingId={1}, " +
                "Name={5}, ExecutionState={2}, TotalWork={3}, WorkCompleted={4}, Deps={6}]",
                Id, OriginatingId, ExecutionState, TotalWork, WorkCompleted, Name,
                Depends.Any() ? string.Join(",", Depends) : "none");
        }

        // dirty but ok for now
        public int GetInt(string message)
        {            
            if (Params.ContainsKey(message))
                return int.Parse(Params[message]);

            if (Params.ContainsKey(message.ToLower()))
                return int.Parse(Params[message.ToLower()]);

            throw new Exception(string.Format("Parameter named {0} not found on task {0}", Name));
        }

        public string GetString(string message)
        {
            if (Params.ContainsKey(message))
                return Params[message];

            if (Params.ContainsKey(message.ToLower()))
                return Params[message.ToLower()];

            throw new Exception(string.Format("Parameter named {0} not found on task {0}", Name));
        }       
    }
}