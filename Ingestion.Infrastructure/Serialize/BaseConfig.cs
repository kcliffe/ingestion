﻿using System.Collections.Generic;

namespace TaskExec.Infrastructure.Serialize
{
    public class BaseConfig
    {
        public string Name { get; set;}
        public List<string> Depends { get; private set; }
        public List<string> QueueUntil { get; set; }
        public List<string> Set { get; set; }
        public List<string> UnSet { get; set; }
        public Dictionary<string, string> Params {get;set;}
    }
}