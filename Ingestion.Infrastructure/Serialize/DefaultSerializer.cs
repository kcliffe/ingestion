using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using RabbitMQ.Client;

namespace Ingestion.Infrastructure
{
	public class DefaultSerializer : ISerializer
	{
		public byte[] Serialize<T> (object subject) where T : class
		{
			using(MemoryStream memoryStream = new MemoryStream())
			using(StreamReader reader = new StreamReader(memoryStream)) {
				DataContractSerializer serializer = new DataContractSerializer(typeof(T));
				serializer.WriteObject(memoryStream, subject);
				memoryStream.Position = 0;
				return memoryStream.ToArray();
			}
		}

		public T Deserialize<T> (byte[] data) where T : class
		{
			using(Stream stream = new MemoryStream()) {
				stream.Write(data, 0, data.Length);
				stream.Position = 0;
				DataContractSerializer deserializer = new DataContractSerializer(typeof(T));
				return deserializer.ReadObject(stream) as T;
			}
		}

		public T Deserialize<T> (string s, bool useTypes = false) where T : class
		{
			using(Stream stream = new MemoryStream()) {
				byte[] data = System.Text.Encoding.UTF8.GetBytes(s);
				stream.Write(data, 0, data.Length);
				stream.Position = 0;
				DataContractSerializer deserializer = new DataContractSerializer(typeof(T));
				return deserializer.ReadObject(stream) as T;
			}
		}
	}
}

