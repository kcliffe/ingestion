﻿using System;
using System.Text;
using Newtonsoft.Json;
using NLog;

namespace TaskExec.Infrastructure.Serialize
{
	public class JsonSerializer : ISerializer
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public byte[] Serialize<T> (object subject, bool useTypes = true) where T : class
		{
			var jsons = new JsonSerializerSettings { 
				TypeNameHandling = useTypes ? TypeNameHandling.Objects : TypeNameHandling.Auto,
				Formatting = Formatting.Indented
			};

			return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject (subject, jsons));
		}

		public T Deserialize<T> (byte[] array) where T : class
		{
			try{
				var jsons = new JsonSerializerSettings { 
					TypeNameHandling = TypeNameHandling.Objects
				};
				return JsonConvert.DeserializeObject<T> (Encoding.UTF8.GetString (array), jsons);
			}
			catch(Exception ex) {
				Logger.Error (ex, "Failed to deserialize object");
				throw;
			}
		}

		public T Deserialize<T> (string s, bool useTypes = true) where T : class
		{
			try {
				var jsons = new JsonSerializerSettings();
				if (useTypes)
				{
					jsons.TypeNameHandling = TypeNameHandling.Objects;
				}

				return JsonConvert.DeserializeObject<T> (s, jsons);
			}
			catch(Exception ex) {
				Logger.Error (ex, "Failed to deserialize object");
				throw;
			}
		}
	}
}

