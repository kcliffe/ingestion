﻿using System.Collections.Generic;
using System.Linq;

namespace TaskExec.Infrastructure.Serialize
{
    public class JobBase
    {
		public JobBase()
		{
			Depends = new List<string>();
		}

        // Must correlate with a task implementation type
        public string Name { get; set; }
        // Any string to describe the task
        public string Description { get; set; }
        // List of tasks this task / job depends on completing before it will execute
        public List<string> Depends { get; private set; }
        // As above but can apply expressions and other constraint to control execution
        public List<string> QueueUntil { get; set; }
        // Set one or more arbitrary values when the task starts execution
        public List<string> Set { get; set; }
        // Clear one or more arbitrary values when the task stops execution
        public List<string> UnSet { get; set; }

        // We used to employ explicit task definition (e.g c# type per message type)
        // We've shifted to the generic form used in the current Wynyard framework.
        // Advantage is that we don't need $type meta in the JSON to deserialize the correct task type
        // Should also be more compatiable when supporting existing TaskExecution signature
        // We could in theory support both - at the cost of more code
        public Dictionary<string, string> Params { get; set; }

        private string _dependsOn;
        /// <summary>
        /// Names of jobs that must execute before this job
        /// </summary>
        /// <value>Comma delimited string of jobs names.</value>
        public string DependsOn
        {
            get { return _dependsOn; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    Depends = value.Split(',').ToList();
                }
                _dependsOn = value;
            }
        }

        /// <summary>
        /// Indicates if this job has dependencies
        /// </summary>
        /// <value><c>true</c> if this instance has dependecies; otherwise, <c>false</c>.</value>
        public bool HasDependecies
        {
            get { return Depends.AnySafe(); }
        }

        public bool HasSetMutexes
        {
            get { return Set.AnySafe(); }
        }

        public bool HasBlockMutexes
        {
            get { return QueueUntil.AnySafe(); }
        }

        public bool HasUnsetMutexes
        {
            get { return UnSet.AnySafe(); }
        }

        public bool HasNoDependecies
        {
            get { return Depends == null || !Depends.Any(); }
        }

        public bool IsDependentOn(string name)
        {
            return Depends.Contains(name);
        }

        public void MergeDependencies(List<string> depends)
        {
            this.Depends.AddRange(depends);
        }

		public void MergeParams(IDictionary<string, string> parameters)
		{
			parameters.Keys.ToList().ForEach (k => {
				if (!this.Params.ContainsKey(k)) {
					this.Params[k] = parameters[k];
				}
			});
		}

        public void RemoveDependency(string name)
        {
            Depends.Remove(name);
        }

        public override string ToString()
        {
            return string.Format("[JobBase: Name={0}, DependsOn={1}, HasDependecies={2}]", Name, DependsOn, HasDependecies);
        }
    }
}

