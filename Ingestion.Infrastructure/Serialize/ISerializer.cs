
namespace TaskExec.Infrastructure
{
	public interface ISerializer
	{
		byte[] Serialize<T>(object subject, bool useTypes = true) where T : class;
		T Deserialize<T> (byte[] array) where T : class;
		T Deserialize<T> (string s, bool useTypes = false) where T : class;
	}
}

