﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TaskExec.Infrastructure.Serialize
{
    /// <summary>
    /// A manifest is a simple list of jobs (currently json formatted).        
    /// </summary>
    public class ManifestDefinition
    {
        public string Id { get; set; }
		public ExecutionState ExecutionState { get; set; } 
        public List<JobDefinition> Jobs { get; set; }

        // KC. Not required i guess once we start actually persisting manifests / jobs / tasks
        public void AssignUniqueIdToSubTasks()
        {
            Jobs.SelectMany(j => j.Tasks).ToList().ForEach(t => t.Id = Guid.NewGuid().ToString());
        }

        public void CopyManifestIdToSubTasks()
        {
            Jobs.ForEach(j => j.Tasks.ForEach(t => t.ManifestId = Id));
        }

        public void CopyJobNameToSubTasks()
        {
            Jobs.ForEach(j => j.Tasks.ForEach(t => t.JobName = j.Name));
        }

        public void MergeJobLevelDependenciesToSubTasks()
        {
            foreach (var j in Jobs)
            {
                if (!j.HasDependecies) continue;
                foreach (var t in j.Tasks)
                {
                    t.MergeDependencies(j.Depends);
                }
            }
        }

		public void MergeJobLevelParamsToSubTasks()
		{
			foreach (var j in Jobs)
			{
				foreach (var t in j.Tasks)
				{
					t.MergeParams (j.Params);
				}
			}
		}

		/// <summary>
		/// Must be performed after param merge
		/// </summary>
        public IEnumerable<TaskDefinition> AllTasks()
        {
            foreach (var j in Jobs)
            {
                foreach (var t in j.Tasks)
                {
					yield return t;
                }
            }
        }
    }
}

