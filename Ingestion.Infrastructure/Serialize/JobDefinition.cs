﻿using System.Collections.Generic;
using System.Linq;

namespace TaskExec.Infrastructure.Serialize
{
    /// <summary>
    /// A job is currently a simple list of tasks
    /// </summary>
    public class JobDefinition : JobBase
    {
        public List<TaskDefinition> Tasks { get; set; }

        public bool IsComplete()
        {
            return Tasks.All(t => t.ExecutionState == ExecutionState.Complete);
        }
    }
}