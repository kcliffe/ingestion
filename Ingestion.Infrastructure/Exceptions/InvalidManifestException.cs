﻿using System;

namespace TaskExec.Infrastructure
{
	public class InvalidManifestException : Exception
	{
		public InvalidManifestException (string message) : base(message)
		{
		}

		public InvalidManifestException (string message, Exception ex) : base(message, ex)
		{
		}
	}
}

