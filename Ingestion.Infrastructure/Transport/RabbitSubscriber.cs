﻿using System;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;
using NLog;
using RabbitMQ.Client;

namespace TaskExec.Infrastructure.Transport
{
	/// <summary>
	/// Typically used in Pub / Sub scenario's with Rabbits Fanout exchange
	/// https://www.rabbitmq.com/tutorials/tutorial-three-dotnet.html
	/// </summary>
	public class RabbitSubscriber<T> where T : class
	{
		private readonly ConnectionFactory _connectionFactory;
		private readonly string _exchangeName;
		private readonly ISerializer _messageSerializer;
		private readonly Action<T> _executor;
		private readonly Signal _signal;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitSubscriber (string hostName, string exchangeName, Action<T> executor, Signal signal)
		{
			_connectionFactory = new ConnectionFactory { HostName = hostName };
			_exchangeName = exchangeName;
			_executor = executor;
			_signal = signal;

			_messageSerializer = new JsonSerializer ();

			Logger.Debug ("Consumer started for host {0}, queue {1}", hostName, exchangeName);
		}

		public void Consume()
		{
			using (var connection = _connectionFactory.CreateConnection())
			{
				using (var channel = connection.CreateModel ()) {
					channel.ExchangeDeclare (_exchangeName, "fanout");

					var queueName = channel.QueueDeclare().QueueName;

					channel.QueueBind (queueName, _exchangeName, "");
					var consumer = new QueueingBasicConsumer (channel);
					channel.BasicConsume (queueName, true, consumer);

					while (_signal.ProcessMessages) {
						var ea = consumer.Queue.Dequeue ();

						try
						{
							var body = ea.Body;					
							var messageObject = _messageSerializer.Deserialize<T> (body);

							_executor.DynamicInvoke(messageObject);

							// acknowledge the successful receipt / processing
							// of the msg. Rabbit-MQ will not remove the message
							// from the queue (or allow it to be consumed) until this
							// ack is either delivered or the consuming process
							// dies.
							channel.BasicAck(ea.DeliveryTag, false);
						}
						catch (Exception ex)
						{
							Logger.Error (ex, "Oops");

							// signal that the message should be retried
							// until TTL.
							channel.BasicNack(ea.DeliveryTag, false, true);    
						}		
					}
				}
			}
		}
	}
}

