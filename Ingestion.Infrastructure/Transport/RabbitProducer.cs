﻿using NLog;
using RabbitMQ.Client;
using System;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Transport
{
	/// <summary>
	/// Puts data onto a Rabbit queue
	/// </summary>
	public class RabbitProducer : IProducer, IDisposable
	{
		private readonly ConnectionFactory _connectionFactory;
		private readonly string _queueName;
		private readonly ISerializer _messageSerializer;
		private readonly dynamic _config;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitProducer (string hostName, string queueName, dynamic @theConfig)
		{
			_connectionFactory = new ConnectionFactory () { HostName = hostName };
			_queueName = queueName;
			_messageSerializer = new JsonSerializer ();
			_config = @theConfig;

			Logger.Debug ("Publisher started for host {0}, queue {1}", hostName, queueName);
		}

		public void Send (object message)
		{
			Send (_queueName, message);
		}

		public void Send(string overrideQueueName, object message)
		{
			using (var connection = _connectionFactory.CreateConnection ()) {
				using (var channel = connection.CreateModel ()) {										
                    // creates implicit (Default) exchange - an exchange with no name
					channel.QueueDeclare (overrideQueueName, _config.Durable, Consts.Params.NonExclusiveQueue, Consts.Params.AutoDeleteOff, null); 
                    
					// mark messages as persistent
					var properties = channel.CreateBasicProperties ();
					properties.Persistent = true; // review

					var body = _messageSerializer.Serialize<TaskDefinition> (message);

                    channel.BasicPublish(Consts.Params.DefaultExchange, overrideQueueName, properties, body);
				}
			}
		}

		public void Dispose ()
		{
			Logger.Debug ("Disposing publisher");            
		}
	}
}

