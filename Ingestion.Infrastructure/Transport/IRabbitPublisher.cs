﻿using System;

namespace TaskExec.Infrastructure.Transport
{
    public interface IRabbitPublisher : IDisposable
    {        
        void Publish (object message);
        void Publish(string exchangeName, object message);
    }
}