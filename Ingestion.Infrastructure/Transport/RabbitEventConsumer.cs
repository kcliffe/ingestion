﻿using System;
using TaskExec.Infrastructure.Serialize;
using NLog;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace TaskExec.Infrastructure.Transport
{
	/// <summary>
	/// Reads message from a single Rabbit queue. Invokes a factory method
	/// </summary>
	public class RabbitEventConsumer<T> : IEventConsumer<T> where T : class
	{
	    private readonly IModel _channel;		
		private readonly string _queueName;
		private readonly ISerializer _messageSerializer;

		public event EventHandler<Custom<T>> MessageRetrieved;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitEventConsumer (string hostName, string queueName, dynamic @theConfig)
		{		    		    
		    var connectionFactory = new ConnectionFactory { HostName = hostName };
			_queueName = queueName;
			var config = @theConfig;

			var connection = connectionFactory.CreateConnection ();
			_channel = connection.CreateModel ();
            _channel.QueueDeclare(queueName, config.Durable, Consts.Params.NonExclusiveQueue, Consts.Params.AutoDeleteOff, null);

			_messageSerializer = new JsonSerializer ();

			Logger.Debug ("Consumer started for host {0}, queue {1}", hostName, queueName);
		}

		public void Consume()
		{
			var consumer = new EventingBasicConsumer(_channel);

            // KC. According to docs, Default TaskScheduler used here, so handlers invoked concurrently on pooled threads
			// TODO. Try catch around Deserialize?
			// TODO. How to flow signal in here?
			consumer.Received += (o, e) => OnMessageRetrieved(new Custom<T> { 
			    // idea. for ack, pass func(s) ack + nack which consmer calls...
			    Message = _messageSerializer.Deserialize<T> (e.Body) 
			});

			_channel.BasicConsume (_queueName, true, consumer);
		}

		public void Stop() {
			//waiter.Set ();
			//waiter.Close ();
		}

		protected virtual void OnMessageRetrieved (Custom<T> messageData)
		{
			var handler = MessageRetrieved;
			if (handler != null)
				handler (this, messageData);
		}
	}

	public class Custom<T> : EventArgs
	{
		public T Message { get; set; }
	}
}

