﻿using System;
using TaskExec.Infrastructure.Execution;
using TaskExec.Infrastructure.Serialize;
using NLog;
using RabbitMQ.Client;

namespace TaskExec.Infrastructure.Transport
{
	/// <summary>
	/// Reads messages from a single Rabbit queue. For each dequeued message a factory method
	/// is used to invoke a handler to process the message.
	/// </summary>
	public class RabbitConsumer<T> : IConsumer where T : class
	{
		private IConnection _connection;
		private IModel _channel;
		private readonly string _queueName;
		private readonly string _hostName;
		private readonly ISerializer _messageSerializer;
		private readonly Action<T> _executor;
		private readonly Signal _signal;
		private readonly dynamic _config;
		private bool _queueCreated;
		private bool disposed;

// ReSharper disable once StaticFieldInGenericType
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitConsumer (string hostName, string queueName, Action<T> executor, Signal signal, dynamic @theConfig)
		{
			_hostName = hostName;
			_queueName = queueName;
			_executor = executor;
			_signal = signal;
			_config = @theConfig;

			_messageSerializer = new JsonSerializer ();
					
			Logger.Debug ("Consumer started for host {0}, queue {1}", hostName, queueName);
		}

		void CreateQueue()
		{
			var connectionFactory = new ConnectionFactory 
			{
				HostName = _hostName
			};
			_connection = connectionFactory.CreateConnection ();
			_channel = _connection.CreateModel ();
			_channel.QueueDeclare (_queueName, _config.Durable, Consts.Params.NonExclusiveQueue, Consts.Params.AutoDeleteOff, Consts.Params.NoArgs);

			// allow the channel to consume 1 unacknowleged message
			// at a time. Prefetch size, prefetch count, global
			_channel.BasicQos (0, 1, false);
			_queueCreated = true;
		}

		public void Consume()
		{
			if (!_queueCreated)
				CreateQueue ();
			
			ConsumeMessagesUntilSignalled();
		}

		private void ConsumeMessagesUntilSignalled()
		{
			var consumer = new QueueingBasicConsumer (_channel);
			_channel.BasicConsume (_queueName, false, consumer);
			var retry = 0;

			while (_signal.ProcessMessages) 
			{
				// block until a message can be dequeue
				var ea = consumer.Queue.Dequeue();
				try {
					var body = ea.Body;
					var messageObject = _messageSerializer.Deserialize<T> (body);
					_executor.DynamicInvoke (messageObject);

					// acknowledge the successful receipt / processing
					// of the msg. Rabbit-MQ will not remove the message
					// from the queue (or allow it to be consumed) until this
					// ack is either delivered or the consuming process dies.
					_channel.BasicAck (ea.DeliveryTag, false);
				}
				// todo. can make this block common
				// for other consumers
				// try { action } catch {}
				catch (Exception ex) 
				{
					var actualEx = ex.InnerException;
					if (actualEx is NonTransientException) 
					{
						// todo: forward message ... somewhere? But for now...
						_channel.BasicAck (ea.DeliveryTag, false);
					} 
					else 
					{
						Logger.Error (actualEx, "Oops");
						retry++;

						if (retry > 3) {
							// todo: forward message ... somewhere... dead letter?
                            Logger.Error("Message has exceeded retry attempts {0}. Message being routed to dead-letter or equiv.", 3);
							_channel.BasicAck (ea.DeliveryTag, false);	
						} 
						else 
						{
							// signal that the message should be retried until TTL.
							_channel.BasicNack (ea.DeliveryTag, false, true);
						}
					}
				}
			}
		}
	
		public void Dispose()
		{ 
			Dispose(true);
			GC.SuppressFinalize(this);           
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
				return; 

			if (disposing) {
				_channel.Dispose ();
				_connection.Dispose ();
			}

			disposed = true;
		}
	}
}

