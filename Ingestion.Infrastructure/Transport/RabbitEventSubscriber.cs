﻿using System;
using TaskExec.Infrastructure.Serialize;
using NLog;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace TaskExec.Infrastructure.Transport
{
	public class RabbitEventSubscriber<T> where T : class
	{
	    //private readonly IConnection connection;
		private readonly IModel _channel;

	    private readonly string _queueName;
		private readonly ISerializer _messageSerializer;

		public event EventHandler<Custom<T>> MessageRetrieved;

// ReSharper disable once StaticFieldInGenericType
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitEventSubscriber (string hostName, string exchangeName)
		{
		    var connectionFactory = new ConnectionFactory { HostName = hostName };
			var connection = connectionFactory.CreateConnection ();
			_channel = connection.CreateModel ();

			_channel.ExchangeDeclare(exchangeName, "fanout");

			_queueName = _channel.QueueDeclare().QueueName;
			_channel.QueueBind(_queueName, exchangeName, Consts.Params.NoRoutingKey);

			_messageSerializer = new JsonSerializer ();

			Logger.Debug ("Consumer started for host {0}, queue {1}", hostName, exchangeName);
		}

		public void Consume()
		{
			var consumer = new EventingBasicConsumer(_channel);

			consumer.Received += (o, e) => OnMessageRetrieved(new Custom<T> () { 
			    // idea. for ack, pass func(s) ack + nack which consmer calls...
			    Message = _messageSerializer.Deserialize<T> (e.Body) 
			});

			_channel.BasicConsume (_queueName, true, consumer);
		}

		protected virtual void OnMessageRetrieved (Custom<T> e)
		{
			var handler = MessageRetrieved;
			if (handler != null)
				handler (this, e);
		}
	}
}

