using System;

namespace TaskExec.Infrastructure.Transport
{
	public interface IConsumer : IDisposable
	{
		void Consume();
	}

	public interface IEventConsumer<T>
	{
		void Consume();
		void Stop ();
		event EventHandler<Custom<T>> MessageRetrieved;
	}
}

