﻿using System;
using RabbitMQ.Client;
using NLog;

namespace Ingestion.Infrastructure
{
	/// <summary>
	/// Reads data from a single Rabbit queue when it's available
	/// </summary>
	public class RabbitConsumer<T> : IConsumer where T : class
	{
		private readonly ConnectionFactory connectionFactory;
		private readonly string queueName;
		private readonly ISerializer messageSerializer;
		private readonly Action<T> executor;
		private readonly Signal signal;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitConsumer (string hostName, string queueName, Action<T> executor, Signal signal)
		{
			connectionFactory = new ConnectionFactory { HostName = hostName };
			this.queueName = queueName;
			this.executor = executor;
			this.signal = signal;

			messageSerializer = new JsonSerializer ();

			Logger.Debug ("Consumer started for host {0}, queue {1}", hostName, queueName);
		}

		public void Consume()
		{
			// KC. May be better to use IDisposable and store class refs to these
			// as something randomly reports
			// System.ObjectDisposedException: The object was used after being disposed.
			// and it's socket related.
			using (var connection = connectionFactory.CreateConnection())
			{
				using (var channel = connection.CreateModel())
				{
					// create a durable queue. A durable queue survives broker restarts
					// but does not garauntee message durability
					// http://rubybunny.info/articles/durability.html
					channel.QueueDeclare (queueName, true, false, false, null);

					// allow the channel to consume 1 unacknowleged message
					// at a time. Prefetch size, prefetch count, global
					channel.BasicQos(0, 1, false);
					var consumer = new QueueingBasicConsumer(channel);
					channel.BasicConsume(queueName, false, consumer);

					while (!signal.Stop)
					{
						// block until a message can be dequeue
						var ea = consumer.Queue.Dequeue();
					
						try
						{
							var body = ea.Body;					
							var messageObject = messageSerializer.Deserialize<T> (body);

							executor.DynamicInvoke(messageObject);

							// acknowledge the successful receipt / processing
							// of the msg. Rabbit-MQ will not remove the message
							// from the queue (or allow it to be consumed) until this
							// ack is either delivered or the consuming process
							// dies.
							channel.BasicAck(ea.DeliveryTag, false);
						}
						catch (Exception ex)
						{
							Logger.Error ("Oops", ex);

							// signal that the message should be retried
							// until TTL.
							channel.BasicNack(ea.DeliveryTag, false, true);    
						}						
					}
				}
			}
		}
	}
}

