﻿namespace TaskExec.Infrastructure.Transport
{
	public interface IHasMessage
	{
		string Message { get; set;}
	}
}

