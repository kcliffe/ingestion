﻿using System;
using TaskExec.Infrastructure.Execution;

namespace TaskExec.Infrastructure.Transport
{
	public enum ConsumerType{
		Rabbit,
		Redis
	}
		
	/// <summary>
	/// Crappy attempt to make the transport agnostic :-)
	/// </summary>
	public static class TransportFactory
	{		
	    public static IConsumer GetConsumer<T>(string hostName, string queueName, Action<T> executor, Signal signal) where T : class
		{
			return new RabbitConsumer<T> (hostName, queueName, executor, signal, new { Durable = false });
		}

		public static IEventConsumer<T> GetEventConsumer<T>(string hostName, string queueName) where T : class
		{
			return new RabbitEventConsumer<T> (hostName, queueName, new { Durable=false });
		}

		public static IProducer GetPublisher(string hostName, string queueName)
		{
			return new RabbitProducer (hostName, queueName, new { Durable=false });
		}
	}
}

