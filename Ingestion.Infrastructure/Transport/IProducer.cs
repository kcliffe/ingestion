namespace TaskExec.Infrastructure.Transport
{
	public interface IProducer
	{
		void Send (object message);
		void Send (string overrideQueueName, object message);
		// void Delete();	// todo. if we've used Send with queuename, do we know what the queue is named?
	}
}