﻿using NLog;
using RabbitMQ.Client;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Transport
{
    /// <summary>
	/// Publisher enabled via RabbitMq fanout exchange
	/// </summary>
	public class RabbitPublisher : IRabbitPublisher
    {
		private readonly ConnectionFactory _connectionFactory;
		private readonly string _exchangeName;
		private readonly ISerializer _messageSerializer;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitPublisher (string hostName, string queueName)
		{
			_connectionFactory = new ConnectionFactory { HostName = hostName };
			_exchangeName = queueName;
			_messageSerializer = new JsonSerializer ();

			Logger.Debug ("Publisher started for host {0}, queue {1}", hostName, queueName);
		}
			
		public void Dispose ()
		{
			Logger.Info ("Disposing...");
		}

		public void Publish (object message)
		{
			Publish (_exchangeName, message);
		}

		public void Publish(string exchangeName, object message)
		{
			using (var connection = _connectionFactory.CreateConnection ()) {
				using (var channel = connection.CreateModel ()) {
					channel.ExchangeDeclare (exchangeName, "fanout");

					var body = _messageSerializer.Serialize<TaskDefinition> (message);

					channel.BasicPublish (exchangeName, "", null, body);
				}
			}
		}
	}
}

