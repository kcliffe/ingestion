﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace TaskExec.Infrastructure
{
	public static class Utils
	{
		public static string GetRootedPath(string relativePath)
		{
			var path = Assembly.GetCallingAssembly().Location;
			var loc = path.IndexOf ("ingestion", StringComparison.CurrentCultureIgnoreCase) 
				+ "ingestion".Length;

			var root = path.Substring (0, loc);
			return Path.Combine (root, relativePath);
		}
	}

	public static class LinqExt
	{
		public static bool AnySafe<TSource>(this IEnumerable<TSource> source)
		{
		    return source != null && source.Any ();
		}

	    public static IEnumerable<TSource> DistinctBy<TSource, TKey>
		(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			var seenKeys = new HashSet<TKey>();
			foreach (TSource element in source)
			{
				if (seenKeys.Add(keySelector(element)))
				{
					yield return element;
				}
			}
		}
	}
}

