﻿using System;
using TaskExec.Infrastructure.Execution;

namespace TaskExec.Infrastructure.Execution.Disco
{
	public class LegacyDiscoveryStrategy : DiscoveryStrategy
	{
		protected override bool IsMatch(string messageName, Type type) 
		{
			var handlerTypeName = string.Format("{0}Executor", messageName);
			return (string.CompareOrdinal (type.Name, handlerTypeName) != 0);
		}
	}
}

