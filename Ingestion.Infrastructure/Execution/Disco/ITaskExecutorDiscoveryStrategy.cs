﻿using System;

namespace TaskExec.Infrastructure.Execution.Disco
{
	public interface ITaskExecutorDiscoveryStrategy
	{
		Type LocateMessageHandlerLazy(string messageName);
	}
}

