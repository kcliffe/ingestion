using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NLog;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution.Disco
{

    public class DiscoveryStrategy : ITaskExecutorDiscoveryStrategy
    {
        private readonly object _lazyLock = new object();
        private static readonly Logger Logger = LogManager.GetLogger("dev_debug");

        // Cache handlers for each message type - obviously one per type currently
        private static ConcurrentDictionary<string, Type> _handlersByMessageType;

		public DiscoveryStrategy()
        {
            _handlersByMessageType = new ConcurrentDictionary<string, Type>();
        }

        /// <summary>
        /// Locate a handler TYPE given a message (name). For now we resolve Message.{Name} => {Name}Executor
        /// Not currently assembly scanning so handler must be loaded into AppDomain.
        /// Downside of current lazy approach is that we may end up scanning for types that we
        /// previsouly haven't found a handler for.
        /// 
        /// Current impl is probably VERY slow since we iterate all types with no qualification.
        /// </summary>
        public Type LocateMessageHandlerLazy(string messageName)
        {
            lock (_lazyLock)
            {
                if (_handlersByMessageType.ContainsKey(messageName))
                {
                    Logger.Debug("Returning handler for message {0} type from cache", messageName);
                    return _handlersByMessageType[messageName];
                }
					
                var folderName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var assemblies = Directory.EnumerateFiles(folderName, "TaskExec.*.dll", SearchOption.TopDirectoryOnly);

                foreach (var f in assemblies)
                {
                    Logger.Debug(string.Format("Enumerating assembly {0} for handler discovery", f));

                    var assembly = Assembly.LoadFrom(f);
                    foreach (var type in assembly.GetTypes())
                    {
                        Logger.Debug(string.Format("Enumerating type {0} for handler discovery", type.Name));

						if (IsMatch(messageName, type))
                            continue;

                        _handlersByMessageType[messageName] = type;
                        Logger.Debug("Handler {0} found for type {1}", type.Name, messageName);
                        return type;
                    }
                }

                return null;
            }
        }  

		protected virtual bool IsMatch(string messageName, Type type) 
		{
			var handlerTypeName = string.Format("{0}Executor", messageName);
			return (string.CompareOrdinal (type.Name, handlerTypeName) != 0);
		}
    }
    
}
