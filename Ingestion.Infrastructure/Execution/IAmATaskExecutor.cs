using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
	public interface IAmATaskExecutor
	{
		void Execute(string executor, TaskDefinition task);
	}
}

