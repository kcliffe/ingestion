﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
	public interface IInvokeTasks
	{
		void Execute(TaskDefinition taskDefinition);
		void AddFilteredJob (string job);
	}
}

