﻿using System;
using Nest;

namespace Ingestion.Infrastructure
{
	public class ElasticLogger : ILog
	{
		private IElasticClient client;

		private string index;
		public string Index {get { return index; }}

		private string url;
		public string Url {get { return url; }}

		public ElasticLogger (string url, string index)
		{
			this.url = url;
			this.index = index.ToLower();

			var node = new Uri(url);
			var settings = new ConnectionSettings(node);
			client = new ElasticClient(settings);

			//var response = client.Map<LogEvent>(m=>m.MapFromAttributes());
			//System.Diagnostics.Debug.WriteLine (response);
		}

		public void Log (string executor, string message, params object[] paramList)
		{
			Internal(new LogEvent { 
				Level = LogLevel.Info,
				Message = string.Format(message, paramList)
			});
		}

		public void Error (string executor, Exception ex, string message, params object[] paramList)
		{
			Internal(new LogEvent { 
				Level = LogLevel.Debug,
				Message = string.Format(message, paramList),
				Ex = ex
			});
		}

		private void Internal(LogEvent logEvent)
		{
			try {
				client.Index (logEvent, i=>i
					.Index(index));
				//result.Wait ();
				}
			catch {
				System.Diagnostics.Debug.WriteLine ("Failed to log!");
			}
		}
	}
}

