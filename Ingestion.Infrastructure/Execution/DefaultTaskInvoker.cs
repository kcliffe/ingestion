﻿using System.Collections.Concurrent;
using System.Linq;
using NLog;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
	/// <summary>
	/// Manage execution of a task when a message has been dequeued.
	/// The TaskExecutor will also apply filters before locating and executing the handler.
	/// These may be useful for some scenario's eg: Don't execute aborted / cancelled tasks.
	/// </summary>
	public class DefaultTaskInvoker : IInvokeTasks
	{
		//private List<Func<TaskDefinition, bool>> _executionFilters; // advanced concept for alter *if* we need it?
		private readonly ConcurrentDictionary<string, byte> _filteredJobs;
		private readonly ITaskFactory _taskFactory;
		private readonly string _execName;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public DefaultTaskInvoker (string execName, Signal signal)
		{
			_execName = execName;
			_taskFactory = new TaskFactory(signal);
			_filteredJobs = new ConcurrentDictionary<string, byte> ();
			// _executionFilters = new List<Func<TaskDefinition, bool>>();
		}	

		public void Execute (TaskDefinition taskDefinition)
		{
//			if (_executionFilters.Any(f => !f(taskDefinition)))
//			{
//				Logger.Debug("Execution of task {0} aborted by filter", taskDefinition.Name);
//			}

			if (_filteredJobs.Keys.Any(j => string.CompareOrdinal(taskDefinition.JobName, j) == 0))
			{
				Logger.Debug("Execution of task {0} aborted by filter", taskDefinition.Name);
				return;
			}

			// Factory could be IOC, actually the NotificationHandler pattern should be used here...
			var executor = _taskFactory.GetTaskImplementation(taskDefinition);
            Logger.Debug("[SOT] Execution of task {0} starting", taskDefinition.Name);
			executor.Execute(_execName, taskDefinition);
            Logger.Debug("[EOT] Execution of task {0} complete", taskDefinition.Name);
		}

		public void AddFilteredJob(string job)
		{
            Logger.Debug("Adding {0} to the filtered job list", job);
            if (string.IsNullOrEmpty(job)) throw new NonTransientException("Job name is required");

			_filteredJobs[job] = 1;
		}
	}
}

