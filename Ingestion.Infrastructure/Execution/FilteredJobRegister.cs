﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace TaskExec.Infrastructure
{
	public interface IFilteredJobRegister
	{
		void AddFilteredJob (string job);
		bool IsJobFiltered (string job);
	}

	/// <summary>
	/// TODO. How / when to remove filter
	/// </summary>
	public class FilteredJobRegister : IFilteredJobRegister
	{
		public ConcurrentDictionary<string, byte> filtered;

		public FilteredJobRegister ()
		{
			filtered = new ConcurrentDictionary<string, byte>();
		}

		public void AddFilteredJob(string job)
		{
			filtered[job] = 1;
		}

		public bool IsJobFiltered(string job)
		{
			return filtered.ContainsKey (job);
		}
	}
}

