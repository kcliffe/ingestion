﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
    public class RestartInfoWrapper
    {
        private readonly ISerializer _serializer;

        public RestartInfoWrapper()
        {
            _serializer = new JsonSerializer();
        }

        public RestartInfoWrapper(ISerializer serializer)
        {
            _serializer = serializer;
        }

        public string Wrap(dynamic restartInfo)
        {
            return _serializer.Serialize<object>(restartInfo);
        }

        public dynamic Unwrap(string wrapped)
        {
            return _serializer.Deserialize<object>(wrapped);
        }
    }
}
