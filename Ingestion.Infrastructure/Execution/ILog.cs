using System;

namespace Ingestion.Infrastructure
{
	public enum LogLevel 
	{
		Debug,
		Info
	}

	public interface ILog
	{
		void Log (string executor, string message, params object[] paramList);
		void Error (string executor, Exception ex, string message, params object[] paramList);
	}

	[ElasticType()]
	public class LogEvent 
	{
		public DateTime When { get; set;}
		[ElasticProperty(Index = FieldIndexOption.NotAnalyzed)]
		public LogLevel Level {	get; set; }
		[ElasticProperty(Index = FieldIndexOption.Analyzed)]
		public string Message {	get; set; }
		[ElasticProperty(Index = FieldIndexOption.Analyzed)]
		public Exception Ex {	get; set; }
	}
}

