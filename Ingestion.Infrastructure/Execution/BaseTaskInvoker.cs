﻿using NLog;
using TaskExec.Infrastructure.Management;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
    public class BaseTaskInvoker : IAmATaskExecutor
    {
        protected static Logger Logger = LogManager.GetCurrentClassLogger();
        protected readonly ManagementNotification Progress;
		protected readonly Signal Signal;

		protected BaseTaskInvoker(Signal signal)
        {
			Progress = new ManagementNotification();
			Signal = signal;
        }

		public virtual void Execute(string executor, TaskDefinition task)
        {
        }       
    }
}

