﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NLog;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Execution.Disco;

namespace TaskExec.Infrastructure.Execution
{
    /// <summary>
    /// Return an implementation for a given Task - a VERY POC attempt!
    ///                                            --------------------
    /// </summary>
    public class TaskFactory : ITaskFactory
    {
        // Used to signal events to handlers 
        private readonly Signal _signal;

        private readonly List<ITaskExecutorDiscoveryStrategy> _taskExecutorDiscoveryStrategies;

        private static readonly Logger Logger = LogManager.GetLogger("dev_debug");

        public TaskFactory(Signal signal)        
        {            
            _taskExecutorDiscoveryStrategies = new List<ITaskExecutorDiscoveryStrategy>
            {
                new DiscoveryStrategy(),
                new LegacyDiscoveryStrategy()
            };
            _signal = signal;
        }

        public IAmATaskExecutor GetTaskImplementation(TaskDefinition message)
        {
			try
			{
			    Type handlerType = null;

			    foreach (var taskExecutorDiscoveryStrategy in _taskExecutorDiscoveryStrategies)
			    {
			        handlerType = taskExecutorDiscoveryStrategy.LocateMessageHandlerLazy(message.Name);
                    if (handlerType != null) break;			        
			    }

			    if (handlerType == null)
			    {
			        Logger.Error("Handler not found for type {0}", message.Name);
			        throw new NonTransientException(string.Format(
			            "Could not locate a handler(class) for message", message.Name));
			    }

			    var task = Activator.CreateInstance(handlerType, _signal);
	            return (IAmATaskExecutor)task;
			}
			catch(Exception ex) 
			{
				throw new TaskExecutorResolverException ("Cannot resolve task", ex);
			}
        }        
    }
}

