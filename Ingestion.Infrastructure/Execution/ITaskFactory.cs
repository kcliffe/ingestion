﻿using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Infrastructure.Execution
{
	public interface ITaskFactory
	{
		IAmATaskExecutor GetTaskImplementation (TaskDefinition message);
	}
}

