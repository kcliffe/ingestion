﻿using System;

namespace Ingestion.Infrastructure
{
	public interface IElasticLogger
	{
		void Log (string executor, string message, params object[] paramList);
		void Error (string executor, Exception ex, string message, params object[] paramList);
	}
}

