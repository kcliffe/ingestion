﻿using System;
using ServiceStack.Messaging.Redis;
using ServiceStack.Redis;
using NLog;

namespace Ingestion.Infrastructure
{
	public class RedisConsumer<T> : IConsumer, IDisposable where T : class
	{

		IRedisClientsManager redisFactory;
		RedisMqServer mqHost;

		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RedisConsumer (Action<T> executor)
		{
			redisFactory = new PooledRedisClientManager ("localhost:6379");
			mqHost = new RedisMqServer (redisFactory, retryCount: 2);

			mqHost.RegisterHandler<T> ((msg) => {
				var msgBody = msg.GetBody();
				executor(msgBody);
				return null;
			});

			Logger.Debug ("Consumer started for host {0}", "localhost:6379");
		}

		public void Consume ()
		{
			mqHost.Start ();
		}

		public void Dispose ()
		{
			if (mqHost != null) {
				mqHost.Dispose ();
			}

			if (redisFactory != null) {
				redisFactory.Dispose ();
			}
		}
	}
}

