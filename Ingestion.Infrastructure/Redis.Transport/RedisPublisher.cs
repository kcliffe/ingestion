﻿using System;
using ServiceStack.Redis;
using ServiceStack.Messaging.Redis;
using ServiceStack.Messaging;
using NLog;

namespace Ingestion.Infrastructure
{
	public class RedisPublisher : IPublisher, IDisposable
	{
		IRedisClientsManager redisFactory;
		RedisMqServer mqHost;
		IMessageQueueClient mqClient;

		private static Logger logger = LogManager.GetCurrentClassLogger();

		public RedisPublisher ()
		{
			redisFactory = new PooledRedisClientManager ("localhost:6379");
			mqHost = new RedisMqServer (redisFactory, retryCount: 2);
			mqClient = mqHost.CreateMessageQueueClient();

			logger.Debug ("Publisher started for host {0}", "localhost:6379");
		}
			
		public void Publish<T> (object message)
		{
			mqClient.Publish (message);
		}

		public void Dispose ()
		{
			if (mqClient != null) {
				mqClient.Dispose ();
			}

			if (mqHost != null) {
				mqHost.Dispose ();
			}

			if (redisFactory != null) {
				redisFactory.Dispose ();
			}
		}
	}
}

