﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using WebSocketSharp;
using WebSocketSharp.Net;
using WebSocketSharp.Server;
using NLog;

namespace Ingestion.Infrastructure
{
	public class WebSocketServerRuntime 
	{
		private static readonly NLog.Logger Logger = LogManager.GetLogger("sockLog");
		WebSocketServer wssv;

		public WebSocketServerRuntime ()
		{
			wssv = new WebSocketServer (4649);
			//var wssv = new WebSocketServer (4649, true);
			//var wssv = new WebSocketServer ("ws://localhost:4649");
			//var wssv = new WebSocketServer ("wss://localhost:4649");
		}

		public void Start()
		{
			#if DEBUG
			// To change the logging level.
			wssv.Log.Level = WebSocketSharp.LogLevel.Trace;

			// To change the wait time for the response to the WebSocket Ping or Close.
			wssv.WaitTime = TimeSpan.FromSeconds (2);
			#endif

			// Add the WebSocket services.
			wssv.AddWebSocketService<NLogSocketForwarder> ("/log");

			wssv.Start ();
			if (wssv.IsListening) {
				Logger.Info ("Listening on port {0}, and providing WebSocket services:", wssv.Port);
				//foreach (var path in wssv.WebSocketServices.Paths)
				//Logger.Info ("- {0}", path);
			}
		}

		public void Stop()
		{
			wssv.Stop ();
		}
	}
}
