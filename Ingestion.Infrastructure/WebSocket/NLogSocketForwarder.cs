﻿using System;
using WebSocketSharp.Server;
using WebSocketSharp;

namespace Ingestion.Infrastructure
{
	public class NLogSocketForwarder : WebSocketBehavior
	{
		protected override void OnMessage (MessageEventArgs e)
		{
			Sessions.Broadcast (e.Data);
		}
	}
}

