﻿using System;
using System.ComponentModel;
using NLog;
using NLog.Config;
using NLog.Targets;
using WebSocketSharp;

namespace Ingestion.Infrastructure
{
	[Target("WebSocket")] 
	public sealed class NLogWebSocket : TargetWithLayout 
	{ 
		private static WebSocket webSocket;

		[DefaultValue("ws://localhost:4649/log")] 
		public string Host { get; set; }

		public NLogWebSocket()
		{
			//webSocket = new WebSocket (Host);
		}
			
		protected override void Write(LogEventInfo logEvent) 
		{ 
			EnsureWebSocket ();

			string logMessage = this.Layout.Render(logEvent); 

			SendTheMessageToSocket(this.Host, logMessage); 
		} 

		private void SendTheMessageToSocket(string host, string message) 
		{ 
			try {
			webSocket.Send (message); 
			}
			catch(Exception ex) {
				Console.WriteLine (ex.Message);
			}
		} 

		private void EnsureWebSocket()
		{
			if (webSocket != null) {
				return;
			}

			webSocket = new WebSocket (Host);
			webSocket.Connect ();
		}
	} 
}

