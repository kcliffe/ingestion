1. Manifest format. All support for this relevant to any approach (Ingestion / akka...)
   - All features of jobs / tasks that are >relevant<. How to test?
3. Wcf API - this requires matching concepts for all interface messages. E.g StatusCodes. All support for this relevant to any approach (Ingestion / akka...)
4. Docs

Known missing features
----------------------
- Restart info
- Manifests
	- Param merge
    - Validation (see code below)
	- Appears to look for input and output dirs and create if not exists. Not sure about this. Currently Task resposibility.
	  See JobBuilder class. Usages indicate that these params can only be placed in:
	    - Input Dir
		- Output Dir
		- Task Params
		- Workflow(?) elements

Known Issues
----------------------
- Cancellation / abort messages may be processed by consumers attaching to an existing exchange/queue. This is
because each process starts it's own consumer. Individual consumers are the guys that filter messages. New consumers
don't know which messages to filter. 

IDEAS
----------------------
- Make BACK CHANNEL durable. That way we can "schedule" updates to the Db based on interval - live manifest info is kept in cache.

Ingestion changes
----------------------
- Messages now non durable. Replaced by "restart info" concept. 


