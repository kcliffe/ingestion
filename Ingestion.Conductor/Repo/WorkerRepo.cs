﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TaskExec.Conductor.Management;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Conductor.TaskDistribution
{
	public class WorkerRepo
	{
		private List<WorkerInfo> workers;
		private readonly JsonSerializer jsonSerializer;
		private readonly string storeBasePath;

		const string workerFileName = "worker_data.json";

		public WorkerRepo (string storeBasePath)
		{
			this.storeBasePath = Utils.GetRootedPath(storeBasePath);
			jsonSerializer = new JsonSerializer ();

			LoadWorkerInfo ();
		}

		public List<WorkerInfo> Workers {
			get {
				return workers;
			}
		}

		public dynamic GetWorkerSummary ()
		{
			return workers;
		}

		public WorkerInfo QueryWorkerByName (string name)
		{
			if (!workers.AnySafe ())
				return null;

			return workers.FirstOrDefault (w=>w.Name == name);
		}
			
		public void AddWorker (WorkerInfo workerInfo)
		{
			if (!workers.Contains(workerInfo)) {
				workers.Add (workerInfo);
				PersistWorkerInfo ();
			}
		}

		public void UpdateWorkerStats (WorkerInfo workerInfo)
		{
			workerInfo.UpdateStats ();
			PersistWorkerInfo ();
		}

		private void PersistWorkerInfo()
		{
			var content = System.Text.UTF8Encoding.UTF8.GetString(
				jsonSerializer.Serialize<List<WorkerInfo>>(workers));

			File.WriteAllText (Path.Combine(storeBasePath, workerFileName), content);
		}

		private void LoadWorkerInfo()
		{
			var workerFilePath = Path.Combine (storeBasePath, workerFileName);
			if (!File.Exists (workerFilePath)) {
				File.Create (workerFilePath);
				return;
			}

			var content = File.ReadAllText (workerFilePath);
			workers = jsonSerializer.Deserialize<List<WorkerInfo>>(content);

			if (!workers.AnySafe ())
				workers = new List<WorkerInfo> ();
		}
	}
}

