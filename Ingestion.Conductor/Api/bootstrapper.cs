﻿using System;
using System.IO;
using Nancy.Bootstrappers.Autofac;
using Autofac;
using Nancy.Diagnostics;
using Nancy;
using Nancy.Conventions;

namespace Ingestion.Conductor
{
	public class Bootstrapper : AutofacNancyBootstrapper
	{
		protected override ILifetimeScope GetApplicationContainer()
		{
			return MainClass.Container;
		}

		protected override void ApplicationStartup(ILifetimeScope container, Nancy.Bootstrapper.IPipelines pipelines)
		{
			StaticConfiguration.DisableErrorTraces = false;

			Conventions.ViewLocationConventions.Add((viewName, model, context) => string.Concat("views", viewName));
		}

		protected override IRootPathProvider RootPathProvider
		{
			get { return new CustomRootPathProvider(); }
		}

		protected override DiagnosticsConfiguration DiagnosticsConfiguration
		{
			get { return new DiagnosticsConfiguration { Password = @"abc" }; }
		}

		protected override void ConfigureConventions(NancyConventions conventions)
		{
			conventions.StaticContentsConventions.Add(
				StaticContentConventionBuilder.AddDirectory("public", @"/assets")
			);

			base.ConfigureConventions(conventions);
		}
	}

	public class CustomRootPathProvider : IRootPathProvider
	{
		public string GetRootPath()
		{
			// FIX THIS!
		    var path = System.Reflection.Assembly.GetExecutingAssembly().Location;
		    var binAt = path.IndexOf("bin");
		    var pre = Path.Combine(path.Substring(0,binAt), "Api/app");

            return pre;
		}
	}
}

