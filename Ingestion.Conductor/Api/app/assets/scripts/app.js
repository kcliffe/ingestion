// Make sure to include the `ui.router` module as a dependency
angular.module('ingestion.management', [
'ui.router',
'ngAnimate'
])
.run(['$rootScope', '$state', '$stateParams',
	function ($rootScope, $state, $stateParams) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		}
	])
.config(	
	[ '$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("home", {
				// Use a url of "/" to set a state as the "index".
				url: "/",
				templateUrl: '/public/views/dash.html',
				controller: 'dashCtrl'
			})
			.state("manifest_log", {
				// Use a url of "/" to set a state as the "index".
				url: "/",
				templateUrl: '/public/views/manifest_log.html',
				controller: 'dashCtrl'
			})
			.state("test", {
				url: "/test",
				templateUrl: '/public/views/test.html',
				controller: 'testCtrl'
			});
		}
	]
);
