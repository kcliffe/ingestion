
angular.module('ingestion.management')
.controller('dashCtrl', ['$scope', 'ingestion.management.service', function ($scope, svc) {
	$scope.dashInfo = {};

	$scope.loadDash = function() {
		svc.getDash().then(function (response) {
                $scope.dashInfo = response.data;
            }, function () {
            	console.log('oops');
                // toastr.error("Failed to retrieve the schema");
            });;
	}

	$scope.loadDash();
}]);
