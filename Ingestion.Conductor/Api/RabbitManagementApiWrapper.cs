﻿using System;
using System.Net;
using System.IO;
using NLog;

namespace Ingestion.Conductor.Management
{
    /// <summary>
    /// Allows us to expose metrics from the underlying transport (RabbitMq in this case)
    /// </summary>
	public class RabbitManagementApiWrapper
	{
		private readonly string baseUrl, username, password;
		private readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public RabbitManagementApiWrapper (string baseUrl, string username, string password)
		{
			this.baseUrl = baseUrl;
			this.username = username;
			this.password = password;
		}

		public string Get(string apiMethod) {
			var webRequest = (HttpWebRequest)WebRequest.Create(baseUrl + apiMethod);
			webRequest.ContentType = "application/json";
			webRequest.Credentials = new NetworkCredential (username, password);

			try {
				return GetBodyFromResponse(webRequest.GetResponse ());
			}
			catch(Exception ex) {
				Logger.Error (ex, "Failed to process the request.");
				throw;
			}
		}

		private static string GetBodyFromResponse(WebResponse response)
		{
			string responseBody;
			using (var responseStream = response.GetResponseStream())
			{
				if (responseStream == null)
				{
					throw new Exception("Response stream was null");
				}
				using (var reader = new StreamReader(responseStream))
				{
					responseBody = reader.ReadToEnd();
				}
			}
			return responseBody;
		}
	}
}
