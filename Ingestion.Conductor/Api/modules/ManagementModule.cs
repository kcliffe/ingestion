﻿using System;
using Ingestion.Conductor.TaskDistribution;
using Nancy;
using System.Collections.Generic;

namespace Ingestion.Conductor.Management
{
	public class Management : NancyModule
	{
		private readonly ProgressRepo progressRepo;
		private readonly WorkerRepo workerRepo;

		const string vHost = "%2F";

		public Management (ProgressRepo progressRepo, WorkerRepo workerRepo)
			: base("ingestion/mgmt")
		{
			var username = "guest"; var password = "guest";
			var rabbitManagementApi = new RabbitManagementApiWrapper("http://localhost:15672/api/", username, password);

			this.progressRepo = progressRepo;
			this.workerRepo = workerRepo;

			ConfigureApi (rabbitManagementApi);
		}

		void ConfigureApi (RabbitManagementApiWrapper rabbitManagementApi)
		{
			Get ["/"] = parameters => {
				return View["index"];
			};

			Get ["/dash"] = parameters =>  {
				return new {
					Status = "ok",
					Manifests = progressRepo.QueryManifestSummary(),
					Workers = workerRepo.GetWorkerSummary()
				};
			};

			Get ["/list/manifests"] = parameters => {
				return progressRepo.QueryActiveManifests();
			};

			Get ["/status/manifest/{manifest}"] = parameters => {
				return progressRepo.QueryManifestState(parameters.manifest);
			};

			Get ["list/workers"] = parameters =>  {
				var workerStates = new List<dynamic>();
				var pingAge = DateTimeOffset.Now;

				workerRepo.Workers.ForEach(w => {
					workerStates.Add(new { 
						State = (pingAge - w.LastPing) > TimeSpan.FromSeconds(30) ? "Bad" : "Good", 
						Worker = w 
					});
				});

				return new {
					Status = "ok",
					Workers = workerStates
				};
			};

//			Get ["/queues/{name}"] = parameters =>  {
//				var queueName = parameters.name;
//				return rabbitManagementApi.Get (
//					string.Format("queues/{0}/{1}",vHost,queueName));
//			};
//
//			Get ["/queues"] = parameters => {
//				return rabbitManagementApi.Get ("queues");
//			};
		}
	}
}