﻿using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using NLog;
using TaskExec.Conductor.Config;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Persist;
using TaskExec.Infrastructure.Serialize;
using System;

namespace TaskExec.Conductor.Management
{
    /// <summary>
    /// Manage the state transitions that occur for manifests / jobs / tasks when 
    /// a) new manifests are created
    /// b) tasks are scheduled for execution
    /// c) tasks compleete execution
    /// d) etc            
    /// </summary>
	public class TaskManagement : IManageTasks
    {
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private readonly JsonSerializer _jsonSerializer;
        private readonly IFileSystem _fileSystem;
	    private readonly IPersistManifests _manifestPersister;
				  
		public IEnumerable<TaskDefinition> CurrentTasks {
			get {
				var jobs = _manifestPersister.GetAll().SelectMany(m => m.Jobs);
				return jobs.SelectMany (j=>j.Tasks).ToList();
			}
		}
			
		public bool QueryManifestIsComplete(string manifestId) 
		{
			return CurrentTasks.All(t => t.ManifestId == manifestId && t.ExecutionState == ExecutionState.Complete);
		}
			
		public TaskManagement (IPersistManifests manifestPersister, IFileSystem fileSystem)
		{
		    _manifestPersister = manifestPersister;
		    _fileSystem = fileSystem;
			_jsonSerializer = new JsonSerializer ();
		}

		/// <summary>
		/// Deserialize the manifest, validate it, perform any additional works (e.g resolve any special  wildcards 
		/// and finally create a persistent version of the manifest which can be scheduled for execution.
		/// </summary>
		/// <param name="manifestPath">Manifest path. When a new manifest notification is received 
		/// it supplies the path to the manifest on disk.</param>
		public void CreateRuntimeManifest (string manifestPath)
		{
			var path = _fileSystem.File.ReadAllText (manifestPath);
			var manifest = _jsonSerializer.Deserialize<ManifestDefinition>(path);
			manifest.Id = string.Format ("{0}_{1}", _fileSystem.Path.GetFileNameWithoutExtension(manifestPath), 
				DateTime.Now.ToFileTime());

			var configurationProcessor = new ConfigurationProcessor(new List<IVariableResolver> {
				new SpecialFolderVariableResolver(),
				new EnvironmentVariableVariableResolver()
				});

			manifest.AssignUniqueIdToSubTasks (); // will be redundant
			manifest.CopyManifestIdToSubTasks (); // ok
			manifest.CopyJobNameToSubTasks (); // ok
			manifest.MergeJobLevelDependenciesToSubTasks(); // ok

			try 
			{
				configurationProcessor.ResolveParams(manifest.AllTasks().Select(t=>t.Params));
				_manifestPersister.Put(manifest);	
			}
			catch (InvalidManifestException ex) 
			{
				Logger.Debug ("Manifest is invalid...", ex.Message);
			}	    
		}

		public IEnumerable<TaskDefinition> GetPendingTasks ()
		{
            return CurrentTasks.Where(t => t.ExecutionState == ExecutionState.Pending).ToList();
		}
			
		public void SetTaskExecuting (string taskId)
		{
			var taskDef = CurrentTasks.First (t => t.Id == taskId);
            SetTaskStatus(taskDef, ExecutionState.Executing);
		}

		public void SetTaskComplete (string taskId)
		{
			var taskDef = CurrentTasks.First (t => t.Id == taskId);
            SetTaskStatus(taskDef, ExecutionState.Complete);

			// if all complete, archive...
			if (CurrentTasks
				.Where (j => j.ManifestId == taskDef.ManifestId)
                .All(t => t.ExecutionState == ExecutionState.Complete))
			{
				// TODO. Complete manifest
			}
		}
			
		public void SetManifestStatus(string manifestId, ExecutionState executionState)
		{
			var manifest = _manifestPersister.GetAll().SingleOrDefault (m=> m.Id == manifestId);
			if (manifest == null)
				throw new KeyNotFoundException ("Manifest {0} does not exist");

			manifest.ExecutionState = ExecutionState.Cancelled;
			manifest.AllTasks().ToList().ForEach(t => {
				t.ExecutionState = ExecutionState.Cancelled;
			});

			_manifestPersister.Put(manifest);	
		}

		public IEnumerable<TaskDefinition> QueryExecutingTasksForJob(string jobName) 
		{
			return QueryTasksWithStateForJob(jobName, ExecutionState.Executing);
		}

		public TaskDefinition QueryTaskById (string taskId)
		{
			return CurrentTasks.First (t => t.Id == taskId);
		}

		// yuck
		public IEnumerable<JobDefinition> QueryExecutingJobs ()
		{
			var executingJobs = CurrentTasks.Where(t => t.ExecutionState == ExecutionState.Executing)
				.Select(t => t.JobName)
				.Distinct();

			return _manifestPersister.GetAll().SelectMany(x => x.Jobs).Where(j => executingJobs.Contains(j.Name));
		}

		public bool QueryJobIsComplete(string jobName) 
		{
			return CurrentTasks.Where(t => t.JobName == jobName).All(t => t.ExecutionState == ExecutionState.Complete);
		}

		public string QueryJobNameByTaskId (string taskId)
		{
			return CurrentTasks.First (x => x.Id == taskId).JobName;
		}

		private IEnumerable<TaskDefinition> QueryTasksWithStateForJob(string jobName,
			ExecutionState state) 
		{
			var executingTasks = CurrentTasks.Where(x => x.ExecutionState == state	&& x.JobName == jobName);
			return executingTasks;
		}

		public double SetTaskProgress (TaskDefinition taskDef, int workUnits, bool incrementWork)
		{
			taskDef.TotalWork = taskDef.TotalWork ?? workUnits;
			if (incrementWork) taskDef.WorkCompleted++;
			double progressPercent = (taskDef.WorkCompleted / taskDef.TotalWork.Value) * 100;

			_manifestPersister.UpdateTask (taskDef);			
			return progressPercent;
		}

        private void SetTaskStatus(TaskDefinition taskDef, ExecutionState state)
		{
			taskDef.ExecutionState = state;
			_manifestPersister.UpdateTask (taskDef);		
		}
			
		private ManifestDefinition GetOwingManifest (string manifestId)
		{
            return _manifestPersister.GetAll().First(m => m.Id == manifestId);
		}
							
	    public void SaveTaskCheckpointInfo(string taskId, string restartInfoValue)
	    {
	        // Impl ...
            // Currently would need to LOCATE task,
            // Set it's RESTARTINFO property
            // Persist entire manifest

            Logger.Debug("Sort of persisting checkpoint info for task {0}", taskId);
	    }
	}
}

