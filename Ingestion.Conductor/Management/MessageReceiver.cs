﻿using System.Collections.Generic;
using System.IO;
using Ingestion.Infrastructure;
using System.Linq;
using NLog;
using System;

namespace Ingestion.Conductor.Management
{
	/// <summary>
	/// Stores the progress of tasks - currently on disk in json format.
	/// 
	/// The store methods are called by the notification receiver which monitors the incoming 
	/// queue.
	/// 
	/// If the store deems a task is complete, we send notification messages to the outgoing (conductor)
	/// queue.
	/// </summary>
	public class MessageReceiver
	{
		private readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private Repo repo;

		public MessageReceiver(IConductor conductor)
		{
			Logger.Info("Management receiver initialised");
		}


	}
}

