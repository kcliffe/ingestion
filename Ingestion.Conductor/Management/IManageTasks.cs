using System.Collections.Generic;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure;

namespace TaskExec.Conductor.Management
{
    public interface IManageTasks
    {        
        void CreateRuntimeManifest (string manifestPath);
		void SetManifestStatus (string manifestName, ExecutionState executionState);
        IEnumerable<TaskDefinition> GetPendingTasks ();
		TaskDefinition QueryTaskById (string taskId);
        IEnumerable<JobDefinition> QueryExecutingJobs ();
        IEnumerable<TaskDefinition> QueryExecutingTasksForJob(string jobName);
		string QueryJobNameByTaskId (string taskId);
        bool QueryJobIsComplete(string jobName);
        void SetTaskExecuting (string taskId);
        void SetTaskComplete (string taskId);
        double SetTaskProgress (TaskDefinition taskDef, int workUnits, bool incrementWork);
        void SaveTaskCheckpointInfo(string taskId, string restartInfoValue);
		bool QueryManifestIsComplete (string manifestId);   
    }
}