﻿namespace TaskExec.Conductor.Management
{
    /*
    [ServiceContract]
    public interface IManagementService
    {
        /// <summary>
        /// Get summary of all or selected jobs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobStatusSummaryResponse GetJobStatusSummary(GetJobStatusSummaryRequest request);

        /// <summary>
        /// Get the parameters of a completed job so that they can be reused in the monitor
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobParametersResponse GetJobParameters(GetJobParametersRequest request);

        /// <summary>
        /// Cancel a workflow job
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        CancelJobResponse CancelJob(CancelJobRequest request);

        /// <summary>
        /// Restart a workflow job
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RestartJobResponse RestartJob(RestartJobRequest request);

        /// <summary>
        /// Delete a workflow job
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        DeleteJobResponse DeleteJob(DeleteJobRequest request);

        /// <summary>
        /// Set failed task to complete
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        SetTaskCompleteResponse SetTaskComplete(SetTaskCompleteRequest request);

        /// <summary>
        /// Initiate a new data load process (workflow)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        InitiateJobResponse InitiateJob(InitiateJobRequest request);

        /// <summary>
        /// Return a sequence of job names
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobNamesResponse GetJobNames(GetJobNamesRequest request);

        /// <summary>
        /// Return a sequence of job definitions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobDefinitionsResponse GetJobDefinitions(GetJobDefinitionsRequest request);

        /// <summary>
        /// Return a list of drives or directories for the machine hosting the dataload service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetFileSystemInfoResponse GetFileSystemInfo(GetFileSystemInfoRequest request);

        /// <summary>
        /// Get information about a specified job by Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobByIdResponse GetJobById(GetJobByIdRequest request);

        [OperationContract]
        GetJobsByIdsResponse GetJobsByIds(GetJobsByIdsRequest request);

        /// <summary>
        /// Get the status of a specified job
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetJobStatusResponse GetJobStatus(GetJobStatusRequest request);

        /// <summary>
        /// Get a list of scheduled jobs for a specified group
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        GetScheduledJobsResponse GetScheduledJobs(GetScheduledJobsRequest request);

        /// <summary>
        /// Create a scheduled job in a specified group
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        CreateScheduledJobResponse CreateScheduledJob(CreateScheduledJobRequest request);

        /// <summary>
        /// Delete a scheduled job given a specified group and job name
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        DeleteScheduledJobResponse DeleteScheduledJob(DeleteScheduledJobRequest request);

        /// <summary>
        /// Run a scheduled job given a specified group and job name
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RunScheduledJobResponse RunScheduledJob(RunScheduledJobRequest request);
    }
    */
}
