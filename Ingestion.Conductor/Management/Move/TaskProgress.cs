﻿using System;

namespace TaskExec.Conductor.Management
{
	public class TaskProgress
	{
		public string TaskId { get;	set; }

		public string TaskName { get; set; }

		public int TotalWork { get;	set; }

		public double WorkCompleted { get; set;	}
	}
}

