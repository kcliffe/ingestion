﻿using System;
using TaskExec.Infrastructure.Management;

namespace TaskExec.Conductor.Management
{
	public class WorkerInfo
	{
		public string Name { get; set; }
		public string MachineName { get; set; }	
		public string InputQueue { get; set; }	
		public DateTimeOffset Registered { get; set; }
		public DateTimeOffset LastPing { get; set; }

		public WorkerInfo ()
		{
		}

		public WorkerInfo (WorkerRegistration r) 
		{
			this.Name = r.Name;
			this.MachineName = r.HostName;
			this.InputQueue = r.InputQueue;

			Registered = LastPing = DateTimeOffset.Now;
		}

		public void UpdateStats()
		{
			Registered = LastPing = DateTimeOffset.Now;
		}

		public override int GetHashCode ()
		{
			unchecked // Overflow is fine, just wrap
			{
				int hash = 17;
				// Suitable nullity checks etc, of course :)
				hash = hash * 23 + Name.GetHashCode();
				hash = hash * 23 + MachineName.GetHashCode();
				hash = hash * 23 + Registered.GetHashCode();
				hash = hash * 23 + LastPing.GetHashCode();
				return hash;
			}
		}

		public override bool Equals ( object obj )
		{
			if (obj == null) {
				return false;
			}
				
			if ( this.GetType () != obj.GetType ()) {
				return false;
			}
			return Equals ((WorkerInfo)obj);
		}

		public bool Equals (WorkerInfo obj)
		{
			if (obj == null) {
				return false;
			}

			if ( ReferenceEquals (this, obj)) {
				return true;
			}

			if ( this.GetHashCode () != obj.GetHashCode())	{
				return false;
			}

			System.Diagnostics.Debug.Assert (
				base.GetType () != typeof ( object ) );

			if ( !base.Equals(obj))	{
				return false;
			}

			return this.Name == obj.Name && this.MachineName == obj.MachineName;
		}
	}
}

