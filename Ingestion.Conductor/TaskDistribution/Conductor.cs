﻿using System;
using System.Linq;
using NLog;
using TaskExec.Conductor.Management;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Management;
using TaskExec.Infrastructure.Serialize;
using TaskExec.Infrastructure.Transport;

namespace TaskExec.Conductor.TaskDistribution
{
    /// <summary>
    /// Manage execution of jobs and tasks contained in the supplied manifest.
    /// </summary>
    /// <remarks>
    /// A manifest can be submitted by submitting a message to the SystemNotificationQueue. Once submitted the manifest will be persisted
    /// at which point any pending tasks in the manifest will be "sent" to the relevant work queue where they will be consumed by a worker.
    /// 
    /// Once the workers have executed a task, they will send progress notifications (again, via the SystemNotificationQueue) which
    /// may enqueueing of any dependent tasks, until all tasks in the manifest are complete.
    /// </remarks>
    public class Conductor : IConductor
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		private readonly IProducer _workerPool;
        private readonly IRabbitPublisher _systemBroadcaster;

		private readonly IManageTasks _taskManagement;		
		private readonly TaskAllocator _taskAllocator;

        public Conductor(IManageTasks taskManagement, TaskAllocator taskAllocator)
        {
            _taskManagement = taskManagement;            
			_taskAllocator = taskAllocator;

            // get a reference to the channel (exchange/queue) used for system notifications
            _systemBroadcaster = new RabbitPublisher(Consts.DefaultHost, Consts.SystemBackChannelQueueName);
            
            // by default, the conductor will push "do work" messages to a group of workers through a queue 
            // specified by the const IngestionConsts.QueueNames.Worker, but this can be changed _per_message_            
            _workerPool = TransportFactory.GetPublisher(Consts.DefaultHost, Consts.QueueNames.Worker);

            // we need to know when tasks have been completed in order to mark them and possibly to "release" more tasks
            IEventConsumer<NotificationBase> systemNotificationQueue = TransportFactory.GetEventConsumer<NotificationBase>(Consts.DefaultHost, Consts.QueueNames.Conductor);			
            systemNotificationQueue.MessageRetrieved += (sender, e) => ProcessNotification(e.Message);
            systemNotificationQueue.Consume();    
        }

        public void LoadManifest(string manifestPath)
        {
            Logger.Info(string.Format("Manifest loaded from {0}. Creating runtime manifest...", manifestPath));

            // read/parse/validate and store (as persistent XEREM classes) the incoming static manifest
            _taskManagement.CreateRuntimeManifest(manifestPath); 
                       
            // and then begin to pass the available tasks to workers / consumers
            BeginQueueingPendingTasks();
        }

        private void ProcessNotification(NotificationBase notificationBase)
        {
            // multiple conditionals here could be replaced by handler<type> pattern but this code is trivial to understand!
            
            // task progress updated (from 0-100%)
            if (notificationBase is TaskProgressUpdate)
            {
                UpdateTaskProgress(notificationBase as TaskProgressUpdate);
                return;
            }
            
            // tasks can save their state (restart info) independently from reporting progress
            if (notificationBase is TaskCheckpointNotification)
            {
                var checkpoint = notificationBase as TaskCheckpointNotification;
                _taskManagement.SaveTaskCheckpointInfo(checkpoint.TaskId, checkpoint.RestartInfoValue);
                return;
            }

            if (notificationBase is CancelSignalNotification)
            {
                CancelManifest(notificationBase as CancelSignalNotification);
                return;
            }

            if (notificationBase is AbortSignalNotification)
            {
                AbortManifest(notificationBase as AbortSignalNotification);
                return;
            }

			if (notificationBase is RestartSignalNotification)
			{
				RestartManifest(notificationBase as RestartSignalNotification);
				return;
			}


            if (!(notificationBase is NewManifestNotification)) throw new Exception("What the hell is this?");
            
            // a new job manifest has been submitted
            LoadManifest((notificationBase as NewManifestNotification).ManifestPath);
        }
        
        /// <summary>
        /// Triggered only when a new manifest is submitted	
        /// </summary>
        public void BeginQueueingPendingTasks()
        {
            Logger.Info("Processing pending tasks");

            int concurrencyReason;
			var nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);

            // process as many jobs as we can until concurrency limits are hit.
            while (nextTask != null)
            {
                QueueTask(nextTask);
				nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);
            }
        }

        private void TaskCompleted(TaskDefinition taskDef)
        {
            // TODO. AfterPipeline. Link to NancyFx Pipeline implementation?
            Logger.Info("Task {0} complete", taskDef.Description);
            _taskManagement.SetTaskComplete(taskDef.Id);

            _taskAllocator.ReleaseGlobalMutexes(taskDef);

            // remove this task from dependency list of all tasks listing it as a dependency
            var allDependentTasks = _taskManagement.GetPendingTasks().Where(t => t.IsDependentOn(taskDef.Name));
            foreach (var depTask in allDependentTasks)
            {
                Logger.Debug("Removing dep on task {0} from task {1}", taskDef.Name, depTask.Name);
                depTask.RemoveDependency(taskDef.Name);
            }

            // if this task completes processing of it's parent job, remove the job from all dependant (job) tasks
            if (_taskManagement.QueryJobIsComplete(taskDef.JobName))
            {
                allDependentTasks = _taskManagement.GetPendingTasks().Where(t => t.IsDependentOn(taskDef.JobName));
                foreach (var depTask in allDependentTasks)
                {
                    Logger.Debug("Removing dep on Job {0} from task {1}", taskDef.JobName, depTask.Name);
                    depTask.RemoveDependency(taskDef.JobName);
                }
            }

            // we're done with this manifest?
			if (_taskManagement.QueryManifestIsComplete(taskDef.ManifestId))
            {
                Logger.Info("All tasks complete.");
                return;
            }

			// there are other tasks that may now be free to go...
            int concurrencyReason;
            var nextTask = _taskAllocator.GetNextTaskAvailableForProcessing(out concurrencyReason);
            if (nextTask != null)
                QueueTask(nextTask);
        }
        
        private void UpdateTaskProgress(TaskProgressUpdate taskProgressUpdate)
        {
            var taskDef = _taskManagement.QueryTaskById(taskProgressUpdate.TaskId);
			var incrementWork = !taskProgressUpdate.InitialiseOnly;
            var progressPercent = _taskManagement.SetTaskProgress(taskDef, taskProgressUpdate.WorkUnits, incrementWork);

            Logger.Debug("Task Id {0}, Name: {1} (Job: {2}) progress updated, {3}%", taskProgressUpdate.TaskId, taskDef.Name, taskDef.JobName, progressPercent);
            if (progressPercent < 100) return;
            TaskCompleted(taskDef);
        }
        
        private void QueueTask(TaskDefinition taskToExecute)
        {
            if (taskToExecute == null) return;

            Logger.Info("Queueing task ({1}){0}", taskToExecute.Id, taskToExecute.Name);

            BeforeTaskExecutes(taskToExecute);

            if (taskToExecute.HasRoute)
            {
                // tag currently used to specify route (queue name) allowing the task to specify a specific cluster of workers
                _workerPool.Send(taskToExecute.Route, taskToExecute);
            }
            else
            {
                _workerPool.Send(taskToExecute);
            }

            // fix. if this fails we need to do something (probably at the notification end) to attempt to clean up
            _taskManagement.SetTaskExecuting(taskToExecute.Id);
        }

        private void BeforeTaskExecutes(TaskDefinition taskDef)
        {
            _taskAllocator.SetGlobalMutexes(taskDef);
        }
        
        public void CancelManifest(CancelSignalNotification cancelSignalNotification)
        {        
			Logger.Info("Cancelling manifest {0}", cancelSignalNotification.ManifestName);
            _systemBroadcaster.Publish(cancelSignalNotification); 

			// we have not set status of all sub tasks. do we need to?
			_taskManagement.SetManifestStatus(cancelSignalNotification.ManifestName, ExecutionState.Cancelled);
        }

        public void AbortManifest(AbortSignalNotification abortSignalNotification)
        {       
            Logger.Info("Abort manifest / job(?)");
            _systemBroadcaster.Publish(abortSignalNotification); 

			// we have not set status of all sub tasks. do we need to?
			_taskManagement.SetManifestStatus(abortSignalNotification.ManifestName, ExecutionState.Aborted);
        }
			
		public void RestartManifest(RestartSignalNotification restartSignalNotification)
        {
			Logger.Info("Abort manifest / job(?)");
			_systemBroadcaster.Publish(restartSignalNotification); 

			// we have not set status of all sub tasks. do we need to?
			_taskManagement.SetManifestStatus(restartSignalNotification.ManifestName, ExecutionState.Aborted);
        }
    }
}