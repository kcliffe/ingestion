﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using TaskExec.Conductor.Management;
using TaskExec.Infrastructure.Persist;
using TaskExec.Infrastructure.Serialize;

namespace TaskExec.Conductor.TaskDistribution
{
	/// <summary>
	/// Reponsible for ensuring the system will only process tasks according to specified constraints.
	/// </summary>
	public class TaskAllocator
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private readonly IManageTasks _taskRepo;
		private readonly Dictionary<string, int> _globalGates;

		private const int MaxConcurrentJobs = 99; // todo. from config
		private const int MaxConcurrentTasks = 99; // todo. from config / manifest

		private readonly object _lockyThing = new object();

		public TaskAllocator (IManageTasks taskRepo)
		{
			_taskRepo = taskRepo;
			_globalGates = new Dictionary<string, int>();
		}

		/// <summary>
		/// Retrieve the next available task. See the sample config files in the solution to see how 
		/// </summary>
		/// <returns>The next task given concurrency throttles.</returns>
		/// <param name="concurrencyReason">The reason (if any) there may be no tasks available.</param>
		public TaskDefinition GetNextTaskAvailableForProcessing(out int concurrencyReason)
		{
            // we lock because probably invoked in fully async manner via .net Task or similar
            // but for "task allocation" we need to ensure serial type access..
		    lock (_lockyThing)
		    {
		        concurrencyReason = 0;

		        var freeTasks = GetTasksNotCurrentlyProcessing();
		        if (freeTasks == null || !freeTasks.Any())
		        {
		            concurrencyReason = -1; // todo Enum
		            return null;
		        }

		        foreach (var freeTask in freeTasks)
		        {
		            // TODO. Pipeline! Just like NancyFx
		            // e.g Add policies (delegates) to collection
		            // TaskExecutionPolicies.Add(ApplyConcurrentJobExecutionPolicy)
		            // e.g then
		            // var freeTasks = TaskExecutionPolicies.Apply()

		            // A task dependent on tasks not yet complete cannot be processed
		            if (ApplyTaskDependencyExecutionPolicy(freeTask) != true)
		            {
		                Logger.Debug("Job {0} blocked because it has dependent tasks not complete",
		                    _taskRepo.QueryJobNameByTaskId(freeTask.Id));
		                continue;
		            }

		            // A maximum number of tasks in seperate jobs can be processing
		            if (ApplyConcurrentJobExecutionPolicy(freeTask) != true)
		            {
		                Logger.Debug("Task {0} blocked by concurrent job execution policy",
		                    _taskRepo.QueryJobNameByTaskId(freeTask.Id));
		                continue;
		            }

		            // A maximum number of tasks in a single job can be processing
		            if (ApplyConcurrentTaskExecutionPolicy(freeTask) != true)
		            {
		                Logger.Debug("Task {0} blocked by concurrent task execution policy", freeTask.Name);
		                continue;
		            }

		            // Only tasks not blocked by currently registered mutexes can be processing
		            if (ApplyGlobalMutexExecutionPolicy(freeTask) != true)
		            {
		                Logger.Debug("Task {0} blocked by dependency or expression (QueueUntil)", freeTask.Name);
		                continue;
		            }

		            // task has passed all policy tests. 
		            return freeTask;
		        }

		        return null;
		    }
		}

		/// <summary>
		/// When a task executes, it may specify a value held in the GlobalGates dictionary  which we "set". 
		/// Other tasks specifying this mutex are blocked. 
		/// </summary>
		public void SetGlobalMutexes(TaskDefinition taskDef)
		{
			if (!taskDef.HasSetMutexes) return;
			lock (_lockyThing)
			{
				Logger.Debug("Setting global mutexes {0}", string.Join(",", taskDef.Set));

				taskDef.Set.ForEach(s =>
					{
						if (_globalGates.ContainsKey(s))
							_globalGates[s]++;
						else
							_globalGates.Add(s, 1);
					});
			}
		}

		/// <summary>
		/// When a task completes, it may specify a value held in the GlobalGates dictionary which we "release" (decrement) 
		/// freeing any tasks blocked by the mutex
		/// </summary>
		/// <param name="taskDef"></param>
		public void ReleaseGlobalMutexes(TaskDefinition taskDef)
		{
			// process any unset values on the task
			if (!taskDef.HasUnsetMutexes) return;

			lock (_lockyThing)
			{
				Logger.Debug("Unsetting global mutexes {0}", string.Join(",", taskDef.UnSet));
				taskDef.UnSet.ForEach(u =>
					{
						if (_globalGates.ContainsKey(u))
						{
							_globalGates[u]--;
						}
					});
			}
		}

		/// <summary>
		/// Return all tasks not yet executed and not blocked by dependencies
		/// </summary>
		/// <returns>The next free task.</returns>
		private IEnumerable<TaskDefinition> GetTasksNotCurrentlyProcessing()
		{
			var tasksNotExecuting = _taskRepo.GetPendingTasks().ToList();

			Logger.Debug("{0} tasks found not marked as executing ({1}).",
				tasksNotExecuting.Count(),
				string.Join(",", tasksNotExecuting.Select(t => t.Name)));

			return tasksNotExecuting;
		}
			
		private bool ApplyTaskDependencyExecutionPolicy(TaskDefinition taskDef)
		{
			// Dependencies are removed from a task(s) as soon as the dependent task
			// has been processed, so we simply check that a task has no dependencies in it's list.
			return taskDef.HasNoDependecies;
		}
			
		private bool ApplyConcurrentJobExecutionPolicy(TaskDefinition taskDef)
		{
			var jobsExecuting = _taskRepo.QueryExecutingJobs();
			var count = jobsExecuting.Count();

			// task for new job? 
			// FIX
			//if (!jobsExecuting.Contains(next.JobName))
			//	count++;

			return count <= MaxConcurrentJobs;
		}

		private bool ApplyConcurrentTaskExecutionPolicy(TaskDefinition taskDef)
		{
			var tasksExecuting = _taskRepo.QueryExecutingTasksForJob(taskDef.JobName);
			return tasksExecuting.Count() <= MaxConcurrentTasks;
		}

		private bool ApplyGlobalMutexExecutionPolicy(TaskDefinition taskDef)
		{
			return !taskDef.HasBlockMutexes /* short circuit */ || TaskPassesTheGates(taskDef);
		}

		/// <summary>
		/// Tasks may specify basic (boolean) expressions in the QueueUntil parameter.
		/// Ensure that this expression evaluates to true before it can be run
		/// </summary>
		private bool TaskPassesTheGates(TaskDefinition taskDef)
		{
			foreach (var qu in taskDef.QueueUntil)
			{
				var gateHasCondition = qu.StartsWith("_");
				if (gateHasCondition)
				{
					var condition = GateCondition.Parse(qu);
					if (!_globalGates.ContainsKey(condition.Gate))
						return false;	// there is a mutex, but it's not set yet?

					var value = _globalGates[condition.Gate];
					Logger.Debug("Checking condition: " + condition);

					switch (condition.OpType)
					{
					case "<":
						if (value >= condition.Operand)
						{
							Logger.Debug("< Failed");
							return false;
						}
						break;

					case "=":
						if (value != condition.Operand)
						{
							Logger.Debug("= Failed");
							return false;
						}
						break;

					case ">":
						if (value <= condition.Operand)
						{
							Logger.Debug("> Failed");
							return false;
						}
						break;
					}
				}
				else // the task simply specifies GlobalGate e.g. "[SchemaUpdate]" which is shorthand for "[SchemaUpdate==0]"
				{
					// completely untested thus far :-)
					if (_globalGates.ContainsKey(qu))
					{
						var pass = _globalGates[qu] == 0;
						Logger.Debug("Gate check without expression {0}", pass ? "suceeded" : "failed");

						if (!pass) return false;
					}
				}

				Logger.Debug("Passed");
			}

			return true;
		}
	}

	/// <summary>
	/// Allows tasks in a manifest to express simple conditions that need to pass
	/// before they can be executed e.g. _LoopTest>4 (see the manifest_condition.json sample manifest)
	/// </summary>
	public class GateCondition
	{
		public string Gate { get; private set; }
		public string OpType { get; private set; }
		public int Operand { get; private set; }

		public static GateCondition Parse(string gate)
		{
			var qu = gate.Substring(1);

			var opOffset = qu.IndexOfAny(new[] { '>', '<', '=' });
			if (opOffset < 0) throw new Exception("Invalid gate condition");

			// TODO. Validation :-)
			var opTyp = qu.Substring(opOffset, 1);
			var theGate = qu.Substring(0, opOffset);
			var operand = int.Parse(qu.Substring(opOffset + 1));

			return new GateCondition { OpType = opTyp, Gate = theGate, Operand = operand };
		}

		public override string ToString()
		{
			return string.Format("[GateCondition: Gate={0}, OpType={1}, Operand={2}]", Gate, OpType, Operand);
		}
	}
}

