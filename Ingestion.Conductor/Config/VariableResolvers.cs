﻿using System;
using System.Text.RegularExpressions;
using TaskExec.Infrastructure;

namespace TaskExec.Conductor.Config
{
	/// <summary>
	/// Resolvers allow the user to embed variable expressions into specific elements
	/// of the manifest. Expressions take the form {resolver}.{reference}
	/// </summary>
	public abstract class BaseVariableResolver : IVariableResolver
	{
		protected Regex WildcardResolverRegex;
		protected MatchCollection Matches;

		public BaseVariableResolver()
		{	
			WildcardResolverRegex = new Regex("([a-z]+).([a-z]+)", RegexOptions.IgnoreCase);
		}

		public abstract bool Handle (string wildcard, out string val);

		protected virtual bool IsMatch(string wildcardExpr, string resolverName) 
		{
			// move validation
			if (!WildcardResolverRegex.IsMatch (wildcardExpr))
				return false;

			Matches = WildcardResolverRegex.Matches(wildcardExpr);
			return String.Compare(Matches[0].Groups[1].Value, resolverName, StringComparison.OrdinalIgnoreCase) == 0;
		}
	}

	public class SpecialFolderVariableResolver : BaseVariableResolver
	{
		public override bool Handle (string wildcard, out string val)
		{
			val = string.Empty;
			if (!base.IsMatch (wildcard, Consts.VariablePrefixes.SpecialFolder))
				return false;

			var folderInWildcardExpr = Matches[0].Groups[2].Value;
			Environment.SpecialFolder folder;
		    if (!Enum.TryParse(folderInWildcardExpr, out folder))
				throw new FormatException(string.Format("Special folder {0} cannot be resolved", folderInWildcardExpr));

		    val = Environment.GetFolderPath (folder);
		    return true;
		}
	}

	public class EnvironmentVariableVariableResolver : BaseVariableResolver
	{
		public override bool Handle (string wildcard, out string val)
		{
			val = string.Empty;
			if (!base.IsMatch (wildcard, Consts.VariablePrefixes.EnvironmentVariable))
				return false;

			var envVarInWildcardExpr = Matches[0].Groups[2].Value;

			val = Environment.GetEnvironmentVariable (envVarInWildcardExpr);
			if (!string.IsNullOrEmpty(val))
				return true;

			throw new FormatException(string.Format("Environment var {0} cannot be resolved", envVarInWildcardExpr));
		}
	}
}

