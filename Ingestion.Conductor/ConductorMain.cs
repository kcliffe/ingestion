﻿using Autofac;
using NLog;
using System.IO.Abstractions;
using TaskExec.Conductor.Management;
using TaskExec.Conductor.TaskDistribution;
using TaskExec.Infrastructure.Persist;
using TaskExec.Persisters.NoPersist;

namespace TaskExec.Conductor
{
	class MainClass
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();		         
		public static IContainer Container;

		public static void Main (string[] args)
		{
			Logger.Info ("\n   ___                _            _             \n  / __\\___  _ __   __| |_   _  ___| |_ ___  _ __ \n / /  / _ \\| '_ \\ / _` | | | |/ __| __/ _ \\| '__|\n/ /__| (_) | | | | (_| | |_| | (__| || (_) | |   \n\\____/\\___/|_| |_|\\__,_|\\__,_|\\___|\\__\\___/|_|   ");
			Logger.Info ("Conductor started.");

			SetUpContainer ();
			//var scope = Container.BeginLifetimeScope ();

            // start the conductor which orchestrates execution of tasks
			Container.Resolve<IConductor>();
            
			// for non blocking transports
			// Console.ReadLine ();
			while (true) {
                //var i = Console.ReadLine ();
                //if (string.CompareOrdinal (i, "q") == 0) {
                //    notifier.Publish (new SignalNotificationBase { AllWorkers = true });
                //}
			}	
		}
			
		// allow sharing the repo with Nancy API
		static void SetUpContainer ()
		{
			var builder = new ContainerBuilder();
						
			builder.RegisterType(typeof(TaskAllocator)).AsSelf ();
            builder.RegisterType(typeof(MemoryPersistence)).As<IPersistManifests>();
		    builder.RegisterType(typeof(TaskManagement)).As<IManageTasks>().SingleInstance();
            builder.RegisterType(typeof(FileSystem)).As<IFileSystem>();
			builder.RegisterType(typeof(TaskDistribution.Conductor)).As<IConductor> ();

			Container = builder.Build();
		}
	}

}
