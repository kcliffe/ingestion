﻿using System;
using Ingestion.Conductor.Management;
using Lex.Db;
using Ingestion.Infrastructure;

namespace Ingestion.Conductor
{
	public class Repository : IDisposable
	{
		private DbInstance dbInstance;

		public Repository (string path)
		{
			dbInstance = new DbInstance (path);

			dbInstance.Map<PendingTaskDefinition> ().Automap (k => k.TaskId);
			dbInstance.Map<TaskDefinition> ().Automap (k => k.Id);
			dbInstance.Initialize();
		}

		public void AddPendingTask(PendingTaskDefinition pendingTaskDef)
		{
			dbInstance.Save<PendingTaskDefinition> (pendingTaskDef);
		}

		public void Dispose()
		{
			if (dbInstance != null)
				dbInstance.Dispose();
		}
	}
}

