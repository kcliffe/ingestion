﻿using System.Threading;
using TaskExec.Infrastructure;
using TaskExec.Infrastructure.Management;
using TaskExec.Infrastructure.Transport;
using NLog;

namespace TaskExec.ManifestLoader
{
	class MainClass
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

		public static void Main (string[] args)
		{
			var publisher = TransportFactory.GetPublisher (Consts.DefaultHost, Consts.QueueNames.Conductor);

            //string manifestPath = Utils.GetRootedPath ("manifests/manifest_cancel.json");
			string manifestPath = Utils.GetRootedPath ("manifests/manifest_condition.json");
			//string manifestPath = Utils.GetRootedPath ("manifests/manifestx_mutex.json");
			//string manifestPath = Utils.GetRootedPath ("manifests/manifestx.json");
			//string manifestPath = Utils.GetRootedPath ("manifests/manifest_stop.json");
			//string manifestPath = Utils.GetRootedPath ("manifests/manifest_small.json");
            //string manifestPath = Utils.GetRootedPath("manifests/manifest_small_generic.json");
			//string manifestPath = Utils.GetRootedPath ("manifests/manifest_c.json");
            //string manifestPath = Utils.GetRootedPath("manifests/requirements/jobs_have_deps.json");
            //string manifestPath = Utils.GetRootedPath("manifests/requirements/max_jobs_per_dataset.json");
            //var manifestPath = Utils.GetRootedPath("manifests/requirements/max_jobs_per_dataset_per_jobtype_x.json");

		    if (string.IsNullOrWhiteSpace(manifestPath)) return;

		    publisher.Send (new NewManifestNotification { ManifestPath = manifestPath });
		    Logger.Info ("Loading new manifest from {0}", manifestPath);

            //CancelProcessing(publisher, manifestPath);
		}

	    private static void CancelProcessing(IProducer publisher, string manifestPath)
	    {
	        Thread.Sleep(1);

			publisher.Send(new CancelSignalNotification("Manny") { AllWorkers = true });
	        Logger.Info("Sending cancellation signal", manifestPath);
	    }
	}
}
