- Intro to arch

1. Basic job with single worker
2. Basic job with multiple tasks, multiple workers?
3. Dependencies
4. Gates
5. Notifications (cancel + ...)
6. Workers / clusters (info via API)
7. Elastic / routes
