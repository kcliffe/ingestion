﻿using System.IO;

namespace TaskExec.Utils
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//var myConnectionString = "server=127.0.0.1;uid=root;database=test;";

//			try
//			{
//				var conn = new MySql.Data.MySqlClient.MySqlConnection();
//				conn.ConnectionString = myConnectionString;
//				conn.Open();
//
//				var results = conn.Query("select * from ns_Person");
//				// (dynamic)results.AsList().First();
//			}
//			catch (MySql.Data.MySqlClient.MySqlException)
//			{
//				
//			}

//			if (args.Length == 0) {
//				CreateRelationshipFile ("50");
//			}
//			else {
//				CreateRelationshipFile (args [0]);
//			}

            //var thing = new AssemblyScanner("dlls")
            //{
            //    IncludeExesInScan = false             
            //};
            //var results = thing.GetScannableAssemblies();					
		}

		static void CreateRelationshipFile (string str)
		{
			int count = 0;
			int max = int.Parse (str);

			using (var sw = new StreamWriter ("relationships.json")) {
				for (int i = 0; i < max; i++) {
					sw.WriteLine (
						"{\"SourceId\":"+(++count)+",\"TargetId\":"+(++count)+"}"
					);
				}
			}
		}
	}
}
